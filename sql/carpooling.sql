--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 11.1

-- Started on 2019-02-10 14:50:39

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 16406)
-- Name: car; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.car (
    id integer NOT NULL,
    userid integer NOT NULL,
    brand character(20) NOT NULL,
    color character(20) NOT NULL,
    type character(20) NOT NULL,
    isaprooved boolean NOT NULL
);


ALTER TABLE public.car OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16414)
-- Name: request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.request (
    id integer NOT NULL,
    userid integer NOT NULL,
    rideid integer NOT NULL,
    status character(20) NOT NULL
);


ALTER TABLE public.request OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16411)
-- Name: ride; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ride (
    id integer NOT NULL,
    userid integer NOT NULL,
    carid integer NOT NULL,
    seatscount integer NOT NULL,
    departuretime date NOT NULL,
    startpoint character(20) NOT NULL,
    endpoint character(20) NOT NULL
);


ALTER TABLE public.ride OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16401)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character(30) NOT NULL,
    rating integer NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 2683 (class 2606 OID 16410)
-- Name: car car_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.car
    ADD CONSTRAINT car_pkey PRIMARY KEY (id);


--
-- TOC entry 2688 (class 2606 OID 16445)
-- Name: request request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_pkey PRIMARY KEY (id);


--
-- TOC entry 2686 (class 2606 OID 16443)
-- Name: ride ride_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ride
    ADD CONSTRAINT ride_pkey PRIMARY KEY (id);


--
-- TOC entry 2681 (class 2606 OID 16405)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2684 (class 1259 OID 16451)
-- Name: fki_user_to_car; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_user_to_car ON public.car USING btree (userid);


--
-- TOC entry 2689 (class 2606 OID 16446)
-- Name: car user_to_car; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.car
    ADD CONSTRAINT user_to_car FOREIGN KEY (userid) REFERENCES public."user"(id) MATCH FULL DEFERRABLE INITIALLY DEFERRED NOT VALID;


-- Completed on 2019-02-10 14:50:40

--
-- PostgreSQL database dump complete
--

