package net.thumbtack.school.carpooling.service;

import net.thumbtack.school.carpooling.enums.RequestStatus;
import net.thumbtack.school.carpooling.enums.ServerErrorCode;
import net.thumbtack.school.carpooling.model.Request;
import net.thumbtack.school.carpooling.model.Ride;
import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.RequestRepository;
import net.thumbtack.school.carpooling.repository.RideRepository;
import net.thumbtack.school.carpooling.repository.SessionRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import net.thumbtack.school.carpooling.request.UserAddRequestAnswerDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateRequestDtoRequest;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for RequestController endpoint
 */
@Service
public class RequestService {

    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private UserRepository userRepository;

    public UserCreateRequestToRideDtoResponse createRequest(String userToken, long rideId) throws CarpoolingException {

        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = session.getUser();
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Ride ride = rideRepository.findById(rideId);
        if (ride == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_RIDE, "Ride");
        }
        if (user.getId() == ride.getUser().getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_OWNER_RIDE, "User");
        }
        Request request = new Request(RequestStatus.SENT);
        request.setUser(user);
        request.setRide(ride);
        requestRepository.save(request);

        return new UserCreateRequestToRideDtoResponse(request.getId(), request.getRequestStatus());
    }

    public UserAddRequestAnswerDtoResponse addRequestAnswer(String userToken, long requestId, UserAddRequestAnswerDtoRequest userAddRequestAnswerDtoRequest) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Request request = requestRepository.findById(requestId);
        if (request == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_REQUEST, "Request");
        }
        if (request.getRide().getUser().getId() != user.getId()) {
            throw new CarpoolingException(ServerErrorCode.NULL_RIDE, "Ride");
        }
        request.setRequestStatus(userAddRequestAnswerDtoRequest.getRequestStatus());
        requestRepository.save(request);

        return new UserAddRequestAnswerDtoResponse(request.getId(), request.getRequestStatus());
    }

    public UserCancelRequestDtoResponse cancelRequest(String userToken, long requestId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = session.getUser();
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Request request = requestRepository.findById(requestId);
        if (request == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_REQUEST, "Request");
        }
        if (request.getUser().getId() != user.getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_NOT_OWNER_REQUEST, "Request");
        }
        request.setRequestStatus(RequestStatus.CANCEL);
        requestRepository.save(request);

        return new UserCancelRequestDtoResponse("Request is cancel");
    }

    public UserUpdateRequestDtoResponse updateRequest(String userToken, long requestId, UserUpdateRequestDtoRequest userUpdateRequestDtoRequest) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Request request = requestRepository.findById(requestId);
        if (request == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_REQUEST, "Request");
        }
        if (request.getRide().getUser().getId() != user.getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_NOT_OWNER_REQUEST, "Request");
        }
        request.setRequestStatus(userUpdateRequestDtoRequest.getRequestStatus());
        requestRepository.save(request);

        return new UserUpdateRequestDtoResponse(request.getId(), request.getRequestStatus());
    }

    public UserGetRequestDtoResponse getRequest(String userToken, long requestId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Request request = requestRepository.findById(requestId);
        if (request == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_REQUEST, "Request");
        }
        if (request.getRide().getUser().getId() != user.getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_NOT_OWNER_REQUEST, "Request");
        }
        return new UserGetRequestDtoResponse(request.getId(), request.getRequestStatus());
    }

    public UserGetAllRequestsDtoResponse getAllRequests(String userToken, long rideId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }

        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }

        Ride ride = rideRepository.findById(rideId);
        if (ride == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_RIDE, "Ride");
        }

        if (user.getId() != ride.getUser().getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_NOT_OWNER_RIDE, "Ride");
        }
        List<Request> requests = requestRepository.findAll(ride.getId());
        if (requests == null) {
            throw new CarpoolingException(ServerErrorCode.EMPTY_REQUEST_HISTORY, "Request");
        }

        UserGetAllRequestsDtoResponse userGetAllRequestsDtoResponse = new UserGetAllRequestsDtoResponse();
        for(Request request:requests){
            userGetAllRequestsDtoResponse.getRequests().add(new RequestDtoResponse(request.getId(),
                    request.getUser().getName(),request.getUser().getEmail(),request.getRequestStatus()));
        }
        return userGetAllRequestsDtoResponse;
    }
}
