package net.thumbtack.school.carpooling.service;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;
import net.thumbtack.school.carpooling.enums.RideStatus;
import net.thumbtack.school.carpooling.enums.ServerErrorCode;
import net.thumbtack.school.carpooling.model.*;
import net.thumbtack.school.carpooling.repository.*;
import net.thumbtack.school.carpooling.request.*;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Service for RideController endpoint
 */
@Service
public class RideService {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private StatisticsRepository statisticsRepository;
    @Autowired
    private PathRepository pathRepository;

    public UserPostRideDtoResponse postRide(UserPostRideDtoRequest userPostRideDtoRequest, String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        sdf.setTimeZone(TimeZone.getDefault());
        Date dateNow = calendar.getTime();
        calendar.setTime(userPostRideDtoRequest.getDepartureTime());
        calendar.add(Calendar.HOUR_OF_DAY, -6);
        Date rideDate = calendar.getTime();
        if (dateNow.after(rideDate)) {
            throw new CarpoolingException(ServerErrorCode.INVALID_DATE, "Date");
        }
        Car car = carRepository.findById(userPostRideDtoRequest.getCarId());
        if (car.getUser().getId() != user.getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_NOT_OWNER_CAR, "carId");
        }
        Ride ride = new Ride(rideDate, userPostRideDtoRequest.getSeatsCount());
        ride.setCar(car);
        ride.setUser(user);
        ride.setRideStatus(RideStatus.WAITING);
        rideRepository.save(ride);
        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
        String start = geometryFactory.createPoint(new Coordinate(userPostRideDtoRequest.getStartPoint().getLatitude(),
                userPostRideDtoRequest.getStartPoint().getLongitude())).toString();
        String end = geometryFactory.createPoint(new Coordinate(userPostRideDtoRequest.getEndPoint().getLatitude(),
                userPostRideDtoRequest.getEndPoint().getLongitude())).toString();
        List<String> paths = pathRepository.findShortPath(pathRepository.getNode(start), pathRepository.getNode(end));
        paths.remove(null);
        if (paths == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_PATH, "Coordinates");
        }
        int pathPart = 0;
        while (pathPart < paths.size()) {
            Path path = new Path(paths.get(pathPart), pathPart);
            path.setRide(ride);
            pathRepository.save(path);
            pathPart++;
        }

        Statistics statistics = statisticsRepository.getStatisticsByUserId(user.getId());
        if (statistics == null) {
            statistics = new Statistics();
            statistics.setRidesCount(1);
            statistics.setUser(user);
        } else {
            int ridesCount = statistics.getRidesCount();
            statistics.setRidesCount(ridesCount++);
        }
        statisticsRepository.save(statistics);


        List<List> ways = new ArrayList<>();
        List<PointDto> pointDtos = new ArrayList<>();
        for (int i = 0; i < paths.size(); i++) {
            for (String elem : paths.get(i).split(",")) {
                String s1 = elem.replace(")", "");
                String s2 = s1.replace("LINESTRING(", "");
                String[] s3 = s2.split("\\s+");
                pointDtos.add (new PointDto(Double.parseDouble(s3[0]), Double.parseDouble(s3[1])));
            }
            ways.add(i, pointDtos);
        }

        return new UserPostRideDtoResponse(ride.getId(), ride.getDepartureTime().toString(), ride.getSeatscount(),
                car.getBrand(), car.getColor(), car.getLicensePlate(), car.getType(), ways, ride.getRideStatus());
    }


    public UserUpdateRideDtoResponse updateRide(UserUpdateRideDtoRequest userUpdateRideDtoRequest, String userToken, long rideId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Car car = carRepository.findById(userUpdateRideDtoRequest.getCarId());
        Ride ride = rideRepository.findById(rideId);
        if (ride == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_RIDE, "Ride");
        }
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        sdf.setTimeZone(TimeZone.getDefault());
        Date dateNow = calendar.getTime();
        calendar.setTime(userUpdateRideDtoRequest.getDepartureTime());
        calendar.add(Calendar.HOUR_OF_DAY, -6);
        Date rideDate = calendar.getTime();
        if (dateNow.after(rideDate)) {
            throw new CarpoolingException(ServerErrorCode.INVALID_DATE, "Date");
        }
        ride.setCar(car);
        ride.setUser(user);
        ride.setDepartureTime(userUpdateRideDtoRequest.getDepartureTime());
        ride.setSeatscount(userUpdateRideDtoRequest.getSeatsCount());
        ride.setRideStatus(userUpdateRideDtoRequest.getRideStatus());
        rideRepository.save(ride);

        return new UserUpdateRideDtoResponse(ride.getId(), ride.getDepartureTime().toString(), ride.getSeatscount(),
                car.getBrand(), car.getColor(), car.getLicensePlate(), car.getType(), ride.getRideStatus());
    }

    public UserUpdateRidePathDtoResponse updateRidePath(UserUpdateRidePathDtoRequest userUpdateRidePathDtoRequest, String userToken, long rideId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Ride ride = rideRepository.findById(rideId);
        if (ride == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_RIDE, "Ride");
        }
        pathRepository.deleteByRideId(ride.getId());
        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
        String start = geometryFactory.createPoint(new Coordinate(userUpdateRidePathDtoRequest.getStartLatitude(),
                userUpdateRidePathDtoRequest.getStartLongitude())).toString();
        String end = geometryFactory.createPoint(new Coordinate(userUpdateRidePathDtoRequest.getEndLatitude(),
                userUpdateRidePathDtoRequest.getEndLongitude())).toString();
        List<String> paths = pathRepository.findShortPath(pathRepository.getNode(start), pathRepository.getNode(end));
        if (paths == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_RIDE, "Ride");
        }
        int pathPart = 0;
        while (pathPart < paths.size()) {
            Path path = new Path(paths.get(pathPart), pathPart);
            path.setRide(ride);
            ride.getPath().add(path);
            pathRepository.save(path);
            pathPart++;
        }

        rideRepository.save(ride);

        return new UserUpdateRidePathDtoResponse(ride.getId(), ride.getPath());
    }

    public UserGetRideDtoResponse getRide(String userToken, long rideId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Ride ride = rideRepository.findById(rideId);
        if (ride == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_RIDE, "Ride");
        }

        return new UserGetRideDtoResponse(ride.getId(), ride.getDepartureTime(), ride.getSeatscount(),
                ride.getCar().getBrand(), ride.getCar().getColor(), ride.getCar().getLicensePlate(), ride.getCar().getType());
    }

    public UserGetAllRidesDtoResponse getAllRides(String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        List<Ride> rides = rideRepository.findAll(user.getId());
        if (rides.size() == 0) {
            throw new CarpoolingException(ServerErrorCode.EMPTY_RIDES_HISTORY, "Ride");
        }

        return new UserGetAllRidesDtoResponse(rides);
    }

    public UserCancelRideDtoResponse cancelRide(String userToken, long rideId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Ride ride = rideRepository.findById(rideId);
        if (ride == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_RIDE, "Ride");
        }
        if (ride.getUser().getId() != user.getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_NOT_OWNER_RIDE, "Ride");
        }
        ride.setRideStatus(RideStatus.CANCEL);
        rideRepository.save(ride);

        return new UserCancelRideDtoResponse(ride.getId(), ride.getDepartureTime(), ride.getSeatscount(),
                ride.getCar().getBrand(), ride.getCar().getColor(), ride.getCar().getLicensePlate(), ride.getCar().getType(),
                ride.getRideStatus());
    }


    public UserGetRidesWithFilterDtoResponse getRidesWithFilter(String userToken, UserGetRidesWithFilterDtoRequest userGetRidesWithFilterDtoRequest) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
        String start = geometryFactory.createPoint(new Coordinate(userGetRidesWithFilterDtoRequest.getStartLatitude(),
                userGetRidesWithFilterDtoRequest.getStartLongitude())).toString();
        String end = geometryFactory.createPoint(new Coordinate(userGetRidesWithFilterDtoRequest.getEndLatitude(),
                userGetRidesWithFilterDtoRequest.getEndLongitude())).toString();
        List<Path> startPath = pathRepository.findWithFilter(start, userGetRidesWithFilterDtoRequest.getRadius());
        if (startPath == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_START_POINT, "Start");
        }
        List<Path> endPath = pathRepository.findWithFilter(end, userGetRidesWithFilterDtoRequest.getRadius());
        if (endPath == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_END_POINT, "End");
        }
        Set<Ride> rides = new HashSet<>();
        for (Path elemStart : startPath) {
            for (Path elemEnd : endPath) {
                if (elemStart.getPathPart() < elemEnd.getPathPart()) {
                    rides.add(rideRepository.findById(elemStart.getRide().getId()).orElse(null));
                }
            }
        }
        UserGetRidesWithFilterDtoResponse userGetRidesWithFilterDtoResponse = new UserGetRidesWithFilterDtoResponse();
        RideDtoResponse rideDtoResponse;
        for (Ride ride : rides) {
            if (ride.getRideStatus() != RideStatus.FINISH) {
                List<Path> paths = ride.getPath();
                List<List> ways = new ArrayList<>();
                List<PointDto> pointDtos = new ArrayList<>();
                for (int i = 0; i < paths.size(); i++) {
                    Path path = paths.get(i);
                    for (String elem : path.getWay().split(",")) {
                        String s1 = elem.replace(")", "");
                        String s2 = s1.replace("LINESTRING(", "");
                        String[] s3 = s2.split("\\s+");
                        pointDtos.add (new PointDto(Double.parseDouble(s3[0]), Double.parseDouble(s3[1])));
                    }
                    ways.add(i, pointDtos);
                }

                rideDtoResponse = new RideDtoResponse(ride.getId(), ride.getDepartureTime().toString(), ride.getSeatscount(),
                        ride.getCar().getBrand(), ride.getCar().getColor(), ride.getCar().getLicensePlate(),
                        ride.getCar().getType(), ways, ride.getRideStatus());
                userGetRidesWithFilterDtoResponse.getRides().add(rideDtoResponse);
            }
        }

        return userGetRidesWithFilterDtoResponse;
    }
}
