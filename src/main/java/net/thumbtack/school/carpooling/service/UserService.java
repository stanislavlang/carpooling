package net.thumbtack.school.carpooling.service;


import net.thumbtack.school.carpooling.enums.ServerErrorCode;
import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.RatingRepository;
import net.thumbtack.school.carpooling.repository.SessionRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import net.thumbtack.school.carpooling.request.UserLoginDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateDtoRequest;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for UserController endpoint
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private RatingRepository ratingRepository;


    public UserLoginDtoResponse saveUser(UserLoginDtoRequest userLoginDtoRequest) {
        User user = userRepository.findByEmail(userLoginDtoRequest.getEmail());
        if(user == null){
            user = new User(userLoginDtoRequest.getEmail(), userLoginDtoRequest.getName());
            user = userRepository.save(user);
        }
        return new UserLoginDtoResponse(user.getId(), user.getEmail(), user.getName());
    }

    public UserUpdateDtoResponse updateUser(String userToken, UserUpdateDtoRequest userUpdateDtoRequest) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        user.setName(userUpdateDtoRequest.getName());
        user.setEmail(userUpdateDtoRequest.getEmail());
        userRepository.save(user);
        return new UserUpdateDtoResponse(user.getId(), user.getEmail(), user.getName());
    }

    public UserDeleteDtoResponse deleteUser(String userToken, long userId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        if (user.getId() != userId) {
            throw new CarpoolingException(ServerErrorCode.WRONG_USER_ID, "userId");
        }
        userRepository.deleteById(userId);
        return new UserDeleteDtoResponse("User is deleted");
    }

    public GetUserDtoResponse getUser(String userToken, long id) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(id);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        int rating = ratingRepository.getAvgRating(id);


        return new GetUserDtoResponse(user.getId(), user.getEmail(), rating, user.getName());
    }

    public GetUserDtoResponse getOwnInfo(String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }




        return new GetUserDtoResponse(user.getId(), user.getEmail(), 0, user.getName());
    }

    public GetAllUsersDtoResponse getAllUsers(String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        List<User> users = userRepository.findAll();

        return new GetAllUsersDtoResponse(users);
    }
}
