package net.thumbtack.school.carpooling.service;

import net.thumbtack.school.carpooling.enums.ServerErrorCode;
import net.thumbtack.school.carpooling.model.Car;
import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.*;
import net.thumbtack.school.carpooling.request.UserAddCarDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateCarDtoRequest;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for CarController endpoint
 */
@Service
public class CarService {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private SessionRepository sessionRepository;

    public UserAddCarDtoResponse addCar(UserAddCarDtoRequest userAddCarDtoRequest, String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Car car = new Car(userAddCarDtoRequest.getBrand(), userAddCarDtoRequest.getColor(), userAddCarDtoRequest.getType(),
                userAddCarDtoRequest.getLicensePlate());
        car.setUser(user);
        carRepository.save(car);
        return new UserAddCarDtoResponse(car.getId(), car.getBrand(), car.getColor(), car.getType(), car.getLicensePlate());
    }

    public ModeratorApproveCarDtoResponse approveCar(String userToken, long carId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Car car = carRepository.findById(carId);
        if (car == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_CAR, "CAR");
        }
        if (user.getId() != car.getUser().getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_NOT_OWNER_CAR, "USER");
        }
        car.setApprooved(true);
        carRepository.save(car);

        return new ModeratorApproveCarDtoResponse(car.getId(), car.getBrand(), car.getColor(), car.getType(), car.getLicensePlate(),
                car.isApprooved());
    }

    public UserUpdateCarDtoResponse updateCar(String userToken, long carId, UserUpdateCarDtoRequest userUpdateCarDtoRequest) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Car car = carRepository.findById(carId);
        if (car == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_CAR, "CAR");
        }
        car.setBrand(userUpdateCarDtoRequest.getBrand());
        car.setColor(userUpdateCarDtoRequest.getColor());
        car.setLicensePlate(userUpdateCarDtoRequest.getLicensePlate());
        car.setType(userUpdateCarDtoRequest.getType());
        carRepository.save(car);
        return new UserUpdateCarDtoResponse(car.getId(), car.getBrand(), car.getColor(), car.getType(), car.getLicensePlate());
    }

    public UserDeleteCarDtoResponse deleteCar(String userToken, long carId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Car car = carRepository.findById(carId);
        if (car == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_CAR, "CAR");
        }
        if (car.getUser().getId() != user.getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_NOT_OWNER_CAR, "USER");
        }
        carRepository.deleteById(carId);
        return new UserDeleteCarDtoResponse("Запись удалена");
    }

    public UserGetCarDtoResponse getCar(String userToken, long carId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Car car = carRepository.findById(carId);
        if (car == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_CAR, "CAR");
        }
        if (car.getUser().getId() != user.getId()) {
            throw new CarpoolingException(ServerErrorCode.USER_IS_NOT_OWNER_CAR, "USER");
        }
        return new UserGetCarDtoResponse(car.getId(), car.getBrand(), car.getColor(), car.getType(), car.getLicensePlate());
    }

    public UserGetAllCarsDtoResponse getCars(String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        List<Car> cars = carRepository.findAll(user.getId());
        if(cars.size() == 0){
            throw new CarpoolingException(ServerErrorCode.USER_DOESNT_HAVE_CARS, "CARS");
        }
        return new UserGetAllCarsDtoResponse(cars);
    }
}
