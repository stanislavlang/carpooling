package net.thumbtack.school.carpooling.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.SessionRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import net.thumbtack.school.carpooling.response.UserLoginDtoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Date;

/**
 * Service for Auth and Login controllers
 */
@Service
public class TokenAuthenticationService {

    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private UserRepository userRepository;


    static final long EXPIRATIONTIME = 864_000_000;

    static final String SECRET = "ThisIsASecret";

    static final String HEADER_STRING = "Access-Token";

    public String addAuthentication(HttpServletResponse response, UserLoginDtoResponse userLoginDtoResponse) {
        String JWT = Jwts.builder().setSubject(userLoginDtoResponse.getEmail())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, SECRET).compact();
        response.addHeader(HEADER_STRING, JWT);
        Session session = new Session(JWT);
        User user = userRepository.findById(userLoginDtoResponse.getId());
        session.setUser(user);
        sessionRepository.save(session);
        return JWT;
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            String user = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody()
                    .getSubject();

            return user != null ? new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList()) : null;
        }
        return null;
    }

    public String getToken(HttpServletRequest request) {
        return request.getHeader(HEADER_STRING);
    }
}
