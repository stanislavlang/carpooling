package net.thumbtack.school.carpooling.service;

import net.thumbtack.school.carpooling.enums.RideStatus;
import net.thumbtack.school.carpooling.enums.ServerErrorCode;
import net.thumbtack.school.carpooling.model.Rating;
import net.thumbtack.school.carpooling.model.Ride;
import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.*;
import net.thumbtack.school.carpooling.request.UserAddRateDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateRateDtoRequest;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for RatingController endpoint
 */
@Service
public class RatingService {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private RequestRepository requestRepository;

    public UserAddRateDtoResponse addRate(UserAddRateDtoRequest userAddRateDtoRequest, String userToken,
                                          long estimatedUserId, long rideId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);

        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User evaluatingUser = userRepository.findById(session.getUser().getId()).orElse(null);
        if (evaluatingUser == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        User estimatedUser = userRepository.findById(estimatedUserId);
        if (estimatedUser == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Ride ride = rideRepository.findById(rideId);
        if (ride == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_RIDE, "RIDE");
        }
        if (ride.getRideStatus() != RideStatus.FINISH) {
            throw new CarpoolingException(ServerErrorCode.RIDE_IS_NOT_OVER, "RIDE");
        }
        Rating rating = new Rating(userAddRateDtoRequest.getRating());

        List<User> rideUsers = requestRepository.getRideUsers(rideId);

        for (User elem : rideUsers) {
            if (elem.getId() == estimatedUserId) {
                rating.setEvaluatingUser(evaluatingUser);
                rating.setEstimatedUser(estimatedUser);
                ratingRepository.save(rating);
            }
        }
        if (rating.getId() == null) {
            throw new CarpoolingException(ServerErrorCode.ESTIMATED_USER_NOT_FOUND, "RIDE");
        }


        return new UserAddRateDtoResponse(rating.getId(), rating.getRating());
    }

    public UserGetRatingDtoResponse getRate(String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = session.getUser();
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Integer rating = ratingRepository.getAvgRating(user.getId());

        return new UserGetRatingDtoResponse(user.getId(), user.getName(), user.getEmail(), rating);
    }

    public UserUpdateRateDtoResponse updateRate(UserUpdateRateDtoRequest userUpdateRateDtoRequest, String userToken, long rateId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User evaluatingUser = userRepository.findById(session.getUser().getId()).orElse(null);
        if (evaluatingUser == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Rating rating = ratingRepository.findById(rateId).orElse(null);
        if (rating == null) {
            throw new CarpoolingException(ServerErrorCode.RATING_DOESNT_EXISTS, "Rating");
        }
        if (evaluatingUser != rating.getEvaluatingUser()) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        User estimatedUser = rating.getEstimatedUser();
        if (estimatedUser == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        rating.setRating(userUpdateRateDtoRequest.getRating());
        ratingRepository.save(rating);

        return new UserUpdateRateDtoResponse(rating.getId(), rating.getRating());
    }


    public UserGetAllRatesDtoResponse getAllRates(String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        List<Rating> rates = ratingRepository.findAll(user.getId());
        if (rates.size() == 0) {
            throw new CarpoolingException(ServerErrorCode.EMPTY_RATING_HISTORY, "Rating");
        }
        return new UserGetAllRatesDtoResponse(rates);
    }

    public UserDeleteRateDtoResponse deleteRate(String userToken, long rateId) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Rating rating = ratingRepository.findById(rateId).orElse(null);
        if (rating == null) {
            throw new CarpoolingException(ServerErrorCode.RATING_DOESNT_EXISTS, "Rating");
        }
        if (rating.getEvaluatingUser().getId() != user.getId()) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        ratingRepository.deleteById(rateId);
        return new UserDeleteRateDtoResponse("Запись удалена");
    }

}
