package net.thumbtack.school.carpooling.service;

import net.thumbtack.school.carpooling.enums.ServerErrorCode;
import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.Statistics;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.SessionRepository;
import net.thumbtack.school.carpooling.repository.StatisticsRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import net.thumbtack.school.carpooling.response.UserDeleteStatisticsDtoResponse;
import net.thumbtack.school.carpooling.response.UserGetStatisticsDtoResponse;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for StatisticsController endpoint
 */
@Service
public class StatisticsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private StatisticsRepository statisticsRepository;


    public UserGetStatisticsDtoResponse getStatistics(String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        Statistics statistics = statisticsRepository.getStatisticsByUserId(user.getId());

        return new UserGetStatisticsDtoResponse(statistics.getId(), user, statistics.getRidesCount());
    }

    public UserDeleteStatisticsDtoResponse deleteStatistics(String userToken) throws CarpoolingException {
        Session session = sessionRepository.findByToken(userToken);
        if (session == null) {
            throw new CarpoolingException(ServerErrorCode.INVALID_SESSION, "SESSION");
        }
        User user = userRepository.findById(session.getUser().getId()).orElse(null);
        if (user == null) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "USER");
        }
        statisticsRepository.deleteByUserId(user.getId());

        return new UserDeleteStatisticsDtoResponse("Statistics is clear");
    }
}
