package net.thumbtack.school.carpooling.serverexception;

import net.thumbtack.school.carpooling.enums.ServerErrorCode;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by RATpr on 28.12.2018.
 */
@EnableWebMvc
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(CarpoolingException.class)
    public ResponseEntity<Map<String, Object>> exceptionHandler(CarpoolingException exception) {
        Map<String, Object> responseException = new HashMap<>();
        responseException.put("errorCode", exception.getErrorCode());
        responseException.put("message", exception.getMessage());
        responseException.put("field", exception.getField());
        return new ResponseEntity<>(responseException, HttpStatus.BAD_REQUEST);
    }


    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, Object> responseException = new HashMap<>();
        responseException.put("errorCode", ServerErrorCode.WRONG_URL);
        responseException.put("message", ServerErrorCode.WRONG_URL.getErrorString());
        responseException.put("field", "URL");
        return new ResponseEntity<Object>(responseException,HttpStatus.NOT_FOUND);
    }

    @Override
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        List<Object> list = new ArrayList<>();
        for (FieldError field : fieldErrors) {
            Map<String, String> responseException = new HashMap<>();
            String[] error = field.getDefaultMessage().split(",");
            responseException.put("errorCode", error[1]);
            responseException.put("message", error[0]);
            responseException.put("field", field.getField());
            list.add(responseException);
        }

        return new ResponseEntity(list, headers, status);
    }


    @Override
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, Object> responseException = new HashMap<>();
        responseException.put("errorCode", ServerErrorCode.NULL_REQUEST);
        responseException.put("message", ServerErrorCode.NULL_REQUEST.getErrorString());
        responseException.put("field", "request");;

        return new ResponseEntity(responseException, headers, status);
    }



}

