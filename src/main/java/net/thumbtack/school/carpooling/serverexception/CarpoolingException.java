package net.thumbtack.school.carpooling.serverexception;

import net.thumbtack.school.carpooling.enums.ServerErrorCode;

import java.util.ArrayList;
import java.util.List;

public class CarpoolingException extends Exception {

    private ServerErrorCode errorCod;
    private List<String> fields = new ArrayList<>();
    private List<CarpoolingException> carpoolingExceptions = new ArrayList<>();
    private String field;
    private List<String> errors;


    public CarpoolingException() {
    }

    public CarpoolingException(ServerErrorCode errorCode, String field) {
        this.errorCod = errorCode;
        this.field = field;
    }

    public CarpoolingException(String field) {
        this.field = field;

    }


    public CarpoolingException(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return errorCod.getErrorString();
    }


    public ServerErrorCode getErrorCode() {
        return errorCod;
    }

    public void setErrorCode(ServerErrorCode errorCode) {
        this.errorCod = errorCode;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<CarpoolingException> getCarpoolingExceptions() {
        return carpoolingExceptions;
    }

    public void setCarpoolingExceptions(List<CarpoolingException> carpoolingExceptions) {
        this.carpoolingExceptions = carpoolingExceptions;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
