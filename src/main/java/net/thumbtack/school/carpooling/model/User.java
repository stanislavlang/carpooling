package net.thumbtack.school.carpooling.model;



import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User entity
 */
@Entity
@Table(name = "\"user\"")
public class User{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String email;
    private String name;
    @OneToMany(mappedBy = "user")
    private List<Car> cars = new ArrayList<>();
    @OneToMany(mappedBy = "user")
    private List<Ride> rides = new ArrayList<>();
    @OneToMany(mappedBy = "user")
    private List<Request> requests = new ArrayList<>();
    @OneToMany(mappedBy = "evaluatingUser")
    private List<Rating> myRating = new ArrayList<>();
    @OneToMany(mappedBy = "estimatedUser")
    private List<Rating> historyRating = new ArrayList<>();
    @OneToOne
    private Session session;
    @OneToOne
    private Statistics statistics;



    public User(String email) {
        this.email = email;
    }

    public User(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public User(Long id, String email, String name) {
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public List<Car> getCars() {
        return cars;
    }

    public User() {
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void setRides(List<Ride> rides) {
        this.rides = rides;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Request> getRequests() {
        return requests;
    }

    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }

    public List<Rating> getHistoryRating() {
        return historyRating;
    }

    public void setHistoryRating(List<Rating> historyRating) {
        this.historyRating = historyRating;
    }

    public List<Rating> getMyRating() {
        return myRating;
    }

    public void setMyRating(List<Rating> myRating) {
        this.myRating = myRating;
    }

    public List<Ride> getRides() {
        return rides;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }
}
