package net.thumbtack.school.carpooling.model;

import javax.persistence.*;

/**
 * Created by User on 03.03.2019.
 */
@Entity
@Table(name = "statistics")
public class Statistics {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int ridesCount;
    @OneToOne
    @JoinColumn(name = "userid")
    private User user;

    public Statistics() {
    }

    public Statistics(Long id, int ridesCount, User user) {
        this.id = id;
        this.ridesCount = ridesCount;
        this.user = user;
    }

    public Statistics(int ridesCount) {
        this.ridesCount = ridesCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRidesCount() {
        return ridesCount;
    }

    public void setRidesCount(int ridesCount) {
        this.ridesCount = ridesCount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
