package net.thumbtack.school.carpooling.model;

import javax.persistence.*;

/**
 * Created by User on 16.03.2019.
 */
@Entity
@Table(name = "path")
public class Path {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String way;
    private Integer pathPart;
    @ManyToOne
    @JoinColumn(name = "rideid")
    private Ride ride;

    public Path() {
    }

    public Path(String way, Integer pathPart) {
        this.way = way;
        this.pathPart = pathPart;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWay() {
        return way;
    }

    public void setWay(String way) {
        this.way = way;
    }

    public Ride getRide() {
        return ride;
    }

    public void setRide(Ride ride) {
        this.ride = ride;
    }

    public Integer getPathPart() {
        return pathPart;
    }

    public void setPathPart(Integer pathPart) {
        this.pathPart = pathPart;
    }

}
