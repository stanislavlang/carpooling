package net.thumbtack.school.carpooling.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Car entity
 */
@Entity
@Table(name = "car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String brand;
    private String color;
    private String type;
    private String licensePlate;
    private boolean isApprooved;
    @ManyToOne
    @JoinColumn(name="userid")
    private User user;

    @OneToMany(mappedBy = "car")
    private List<Ride> rides = new ArrayList<>();
    public Car() {
    }

    public Car(String brand, String color, String type, String licensePlate) {
        this.brand = brand;
        this.color = color;
        this.type = type;
        this.licensePlate = licensePlate;
    }

    public List<Ride> getRides() {
        return rides;
    }

    public Long getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public boolean isApprooved() {
        return isApprooved;
    }

    public void setApprooved(boolean approoved) {
        isApprooved = approoved;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;

        Car car = (Car) o;

        if (isApprooved() != car.isApprooved()) return false;
        if (getId() != null ? !getId().equals(car.getId()) : car.getId() != null) return false;
        if (getBrand() != null ? !getBrand().equals(car.getBrand()) : car.getBrand() != null) return false;
        if (getColor() != null ? !getColor().equals(car.getColor()) : car.getColor() != null) return false;
        if (getType() != null ? !getType().equals(car.getType()) : car.getType() != null) return false;
        if (getLicensePlate() != null ? !getLicensePlate().equals(car.getLicensePlate()) : car.getLicensePlate() != null)
            return false;
        return getUser() != null ? getUser().equals(car.getUser()) : car.getUser() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getBrand() != null ? getBrand().hashCode() : 0);
        result = 31 * result + (getColor() != null ? getColor().hashCode() : 0);
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getLicensePlate() != null ? getLicensePlate().hashCode() : 0);
        result = 31 * result + (isApprooved() ? 1 : 0);
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
        return result;
    }
}
