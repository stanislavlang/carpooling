package net.thumbtack.school.carpooling.model;

import javax.persistence.*;

/**
 * Rating entity
 */
@Entity
@Table(name = "rating")
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "estimated_userid")
    private User estimatedUser;
    @ManyToOne
    @JoinColumn(name = "evaluating_userid")
    private User evaluatingUser;
    private int rating;

    public Rating() {
    }

    public Rating(int rating) {
        this.rating = rating;
    }

    public User getEstimatedUser() {
        return estimatedUser;
    }

    public void setEstimatedUser(User estimatedUser) {
        this.estimatedUser = estimatedUser;
    }

    public User getEvaluatingUser() {
        return evaluatingUser;
    }

    public void setEvaluatingUser(User evaluatingUser) {
        this.evaluatingUser = evaluatingUser;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rating)) return false;

        Rating rating1 = (Rating) o;

        if (getRating() != rating1.getRating()) return false;
        if (getId() != null ? !getId().equals(rating1.getId()) : rating1.getId() != null) return false;
        if (getEstimatedUser() != null ? !getEstimatedUser().equals(rating1.getEstimatedUser()) : rating1.getEstimatedUser() != null)
            return false;
        return getEvaluatingUser() != null ? getEvaluatingUser().equals(rating1.getEvaluatingUser()) : rating1.getEvaluatingUser() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getEstimatedUser() != null ? getEstimatedUser().hashCode() : 0);
        result = 31 * result + (getEvaluatingUser() != null ? getEvaluatingUser().hashCode() : 0);
        result = 31 * result + getRating();
        return result;
    }
}
