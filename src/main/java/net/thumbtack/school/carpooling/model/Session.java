package net.thumbtack.school.carpooling.model;



import javax.persistence.*;

/**
 * Session entity
 */
@Entity
@Table(name="session")
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String token;
    @OneToOne
    @JoinColumn(name = "userid")
    private User user;



    public Session() {
    }


    public Session(String token) {
        this.token = token;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;

        Session session = (Session) o;

        if (getId() != null ? !getId().equals(session.getId()) : session.getId() != null) return false;
        return getToken() != null ? getToken().equals(session.getToken()) : session.getToken() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getToken() != null ? getToken().hashCode() : 0);
        return result;
    }
}
