package net.thumbtack.school.carpooling.model;

import net.thumbtack.school.carpooling.enums.RideStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Ride entity
 */
@Entity
@Table(name = "ride")
public class Ride {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date departureTime;
    private int seatsCount;
    @ManyToOne
    @JoinColumn(name = "userid")
    private User user;
    @ManyToOne
    @JoinColumn(name = "carid")
    private Car car;
    @OneToMany(mappedBy = "ride")
    private List<Request> requests = new ArrayList<>();
    @OneToMany(mappedBy = "ride")
    private List<Path> path = new ArrayList<>();
    private RideStatus rideStatus;

    public Ride() {
    }

    public Ride(Date departureTime, int seatsCount) {
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
    }

    public Ride(Long id, Date departureTime, int seatsCount, User user, Car car, List<Path> path, RideStatus rideStatus) {
        this.id = id;
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
        this.user = user;
        this.car = car;
        this.path = path;
        this.rideStatus = rideStatus;
    }

    public Ride(Date departureTime, int seatsCount, RideStatus rideStatus) {
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
        this.rideStatus = rideStatus;
    }

    public RideStatus getRideStatus() {
        return rideStatus;
    }

    public void setRideStatus(RideStatus rideStatus) {
        this.rideStatus = rideStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public int getSeatscount() {
        return seatsCount;
    }

    public void setSeatscount(int seatsCount) {
        this.seatsCount = seatsCount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public List<Request> getRequests() {
        return requests;
    }

    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }



    public List<Path> getPath() {
        return path;
    }

    public void setPath(List<Path> path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ride)) return false;

        Ride ride = (Ride) o;

        if (seatsCount != ride.seatsCount) return false;
        if (getId() != null ? !getId().equals(ride.getId()) : ride.getId() != null) return false;
        if (getDepartureTime() != null ? !getDepartureTime().equals(ride.getDepartureTime()) : ride.getDepartureTime() != null)
            return false;
        if (getUser() != null ? !getUser().equals(ride.getUser()) : ride.getUser() != null) return false;
        if (getCar() != null ? !getCar().equals(ride.getCar()) : ride.getCar() != null) return false;
        if (getRequests() != null ? !getRequests().equals(ride.getRequests()) : ride.getRequests() != null)
            return false;
        return getPath() != null ? getPath().equals(ride.getPath()) : ride.getPath() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getDepartureTime() != null ? getDepartureTime().hashCode() : 0);
        result = 31 * result + seatsCount;
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
        result = 31 * result + (getCar() != null ? getCar().hashCode() : 0);
        result = 31 * result + (getRequests() != null ? getRequests().hashCode() : 0);
        result = 31 * result + (getPath() != null ? getPath().hashCode() : 0);
        return result;
    }
}
