package net.thumbtack.school.carpooling.model;

import net.thumbtack.school.carpooling.enums.RequestStatus;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Request Entity
 */
@Entity
@Table(name = "request")
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Enumerated(EnumType.STRING)
    private RequestStatus requestStatus;
    @ManyToOne
    @JoinColumn(name="rideid")
    private Ride ride;
    @ManyToOne
    @JoinColumn(name="userid")
    private User user;


    public Request() {
    }

    public Request(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Ride getRide() {
        return ride;
    }

    public void setRide(Ride ride) {
        this.ride = ride;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Request)) return false;

        Request request = (Request) o;

        if (getId() != request.getId()) return false;
        if (getRequestStatus() != request.getRequestStatus()) return false;
        if (getRide() != null ? !getRide().equals(request.getRide()) : request.getRide() != null) return false;
        return getUser() != null ? getUser().equals(request.getUser()) : request.getUser() == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getRequestStatus() != null ? getRequestStatus().hashCode() : 0);
        result = 31 * result + (getRide() != null ? getRide().hashCode() : 0);
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
        return result;
    }
}
