package net.thumbtack.school.carpooling.request;

/**
 * DTO request for method createUser in UserController endpoint
 */
public class UserLoginDtoRequest {
    private String email;
    private String name;
    private int rating;

    public UserLoginDtoRequest() {
    }

    public UserLoginDtoRequest(String email, int rating) {
        this.email = email;
        this.rating = rating;
    }

    public UserLoginDtoRequest(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
