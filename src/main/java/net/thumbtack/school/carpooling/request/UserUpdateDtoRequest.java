package net.thumbtack.school.carpooling.request;

/**
 * DTO request for method updateUser in UserController endpoint
 */
public class UserUpdateDtoRequest {
    private String email;
    private String name;


    public UserUpdateDtoRequest() {
    }


    public UserUpdateDtoRequest(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
