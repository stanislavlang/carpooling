package net.thumbtack.school.carpooling.request;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;
import net.thumbtack.school.carpooling.enums.RideStatus;

import java.util.Date;


/**
 * DTO request for method updateRide in RideController endpoint
 */
public class UserUpdateRideDtoRequest {
    private Date departureTime;
    private int seatsCount;
    private long carId;
    private RideStatus rideStatus;

    public UserUpdateRideDtoRequest() {
    }

    public UserUpdateRideDtoRequest(Date departureTime, int seatsCount, long carId) {
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
        this.carId = carId;
    }
    public UserUpdateRideDtoRequest(Date departureTime, int seatsCount, long carId, RideStatus rideStatus) {
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
        this.carId = carId;
        this.rideStatus = rideStatus;
    }

    public RideStatus getRideStatus() {
        return rideStatus;
    }

    public void setRideStatus(RideStatus rideStatus) {
        this.rideStatus = rideStatus;
    }

    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public int getSeatsCount() {
        return seatsCount;
    }

    public void setSeatsCount(int seatsCount) {
        this.seatsCount = seatsCount;
    }

}
