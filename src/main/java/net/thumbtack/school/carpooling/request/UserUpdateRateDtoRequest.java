package net.thumbtack.school.carpooling.request;

/**
 * DTO request for method updateRate in RatingController endpoint
 */
public class UserUpdateRateDtoRequest {
    private int rating;

    public UserUpdateRateDtoRequest() {
    }

    public UserUpdateRateDtoRequest(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
