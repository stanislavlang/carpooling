package net.thumbtack.school.carpooling.request;

/**
 * Created by User on 24.03.2019.
 */
public class PointDto {
    private double latitude;
    private double longitude;


    public PointDto() {
    }

    public PointDto(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "{" +
                "latitude:" + latitude +
                ", longitude: " + longitude +
                '}';
    }
}
