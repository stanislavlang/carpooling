package net.thumbtack.school.carpooling.request;

import net.thumbtack.school.carpooling.enums.RequestStatus;

/**
 * DTO for  @PutMapping(value = "/api/request/{id}" Request Controller
 */
public class UserUpdateRequestDtoRequest {
    private RequestStatus requestStatus;

    public UserUpdateRequestDtoRequest() {
    }

    public UserUpdateRequestDtoRequest(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }
}
