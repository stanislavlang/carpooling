package net.thumbtack.school.carpooling.request;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;

import java.util.Date;


/**
 * DTO request for method createRide in UserController endpoint
 */
public class UserPostRideDtoRequest {
    private Date departureTime;
    private int seatsCount;
    private PointDto startPoint;
    private PointDto endPoint;
    private long carId;

    public UserPostRideDtoRequest() {
    }

    public UserPostRideDtoRequest(Date departureTime, int seatsCount, double startLatitude, double startLongitude, double endLatitude,
                                     double endLongitude, long carId) {
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
        this.startPoint = new PointDto(startLatitude,startLongitude);
        this.endPoint = new PointDto(endLatitude,endLongitude);
        this.carId = carId;
    }

    public UserPostRideDtoRequest(Date departureTime, int seatsCount, PointDto startPoint, PointDto endPoint, long carId) {
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.carId = carId;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public int getSeatsCount() {
        return seatsCount;
    }

    public void setSeatsCount(int seatsCount) {
        this.seatsCount = seatsCount;
    }

    public PointDto getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(PointDto startPoint) {
        this.startPoint = startPoint;
    }

    public PointDto getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(PointDto endPoint) {
        this.endPoint = endPoint;
    }

    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }
}
