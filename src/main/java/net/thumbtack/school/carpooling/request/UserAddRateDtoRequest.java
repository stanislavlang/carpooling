package net.thumbtack.school.carpooling.request;

/**
 * DTO request for method addRate in UserController endpoint
 */
public class UserAddRateDtoRequest {
    private int rating;

    public UserAddRateDtoRequest() {
    }

    public UserAddRateDtoRequest(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
