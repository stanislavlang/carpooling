package net.thumbtack.school.carpooling.request;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;

/**
 * DTO request for method getRidesWithFilter in RideController endpoint
 */
public class UserGetRidesWithFilterDtoRequest {
    private double startLatitude;
    private double startLongitude;
    private double endLatitude;
    private double endLongitude;
    private double radius;


    public UserGetRidesWithFilterDtoRequest() {
    }

    public UserGetRidesWithFilterDtoRequest(double startLatitude, double startLongitude, double endLatitude,
                                            double endLongitude, double radius) {
        this.startLatitude = startLatitude;
        this.startLongitude = startLongitude;
        this.endLatitude = endLatitude;
        this.endLongitude = endLongitude;
        this.radius = radius;
    }


    public double getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(double startLatitude) {
        this.startLatitude = startLatitude;
    }

    public double getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(double startLongitude) {
        this.startLongitude = startLongitude;
    }

    public double getEndLatitude() {
        return endLatitude;
    }

    public void setEndLatitude(double endLatitude) {
        this.endLatitude = endLatitude;
    }

    public double getEndLongitude() {
        return endLongitude;
    }

    public void setEndLongitude(double endLongitude) {
        this.endLongitude = endLongitude;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
