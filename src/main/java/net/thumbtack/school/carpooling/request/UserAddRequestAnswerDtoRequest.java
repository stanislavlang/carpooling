package net.thumbtack.school.carpooling.request;

import net.thumbtack.school.carpooling.enums.RequestStatus;


/**
 * Created by User on 04.03.2019.
 */
public class UserAddRequestAnswerDtoRequest {

    private RequestStatus requestStatus;

    public UserAddRequestAnswerDtoRequest() {
    }

    public UserAddRequestAnswerDtoRequest(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }
}
