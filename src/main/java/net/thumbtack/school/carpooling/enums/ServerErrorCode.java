package net.thumbtack.school.carpooling.enums;

/**
 * Enum for custom exception answer
 */
public enum ServerErrorCode {
    WRONG_URL("URL DOESNT EXIST"),
    NULL_REQUEST("Empty request doesnt allow"),
    NULL_USER("User is not authorized"),
    NULL_CAR("Car doesnt exist"),
    USER_IS_NOT_OWNER_CAR("User is not owner of car"),
    INVALID_SESSION("Invalid session"),
    USER_DOESNT_HAVE_CARS("User doesnt have any cars"),
    RATING_DOESNT_EXISTS("Incorrect rating ID"),
    EMPTY_RATING_HISTORY("History of rates is null"),
    NULL_RIDE("Incoreect ride ID"),
    USER_IS_NOT_OWNER_REQUEST("User is not owner of request"),
    EMPTY_REQUEST_HISTORY("History of requests is null"),
    NULL_PATH("Wrong coordinates"),
    EMPTY_RIDES_HISTORY("History of rides is null"),
    USER_IS_NOT_OWNER_RIDE("User is not owner of ride"),
    NULL_START_POINT("There are no ways for this start point"),
    NULL_END_POINT("There are no ways for this end point"),
    WRONG_USER_ID("Wrong user id"),
    USER_IS_OWNER_RIDE("User can sent requests to his rides"),
    RIDE_IS_NOT_OVER("Ride is not over"),
    ESTIMATED_USER_NOT_FOUND("Estimated user is not found in this ride"),
    INVALID_DATE("Ride date can not be before current time");

    private String errorString;

    private ServerErrorCode(String errorString){
        this.errorString = errorString;
    }
    public String getErrorString(){
        return errorString;
    }
}
