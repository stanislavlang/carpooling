package net.thumbtack.school.carpooling.enums;

/**
 * Enum for Ride entity
 */
public enum RideStatus {
    WAITING,FINISH,IN_PROGRESS, CANCEL;

}
