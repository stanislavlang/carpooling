package net.thumbtack.school.carpooling.enums;

/**
 * Enum for Request entity
 */
public enum RequestStatus {
    ACCEPT,DECLINE,SENT, CANCEL;

}
