package net.thumbtack.school.carpooling.repository;

import net.thumbtack.school.carpooling.model.Statistics;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository interface for Statistics entity
 */
@Repository("statisticsRepository")
public interface StatisticsRepository extends CrudRepository<Statistics,Long> {
    Statistics save(Statistics statistics);
    @Query("Select s FROM Statistics s where s.user.id = :id")
    Statistics getStatisticsByUserId(@Param("id") long userId);
    @Query("Delete FROM Statistics s where s.user.id = :id")
    void deleteByUserId(@Param("id") long id);
    List<Statistics> findAll();
    void deleteAll();
}
