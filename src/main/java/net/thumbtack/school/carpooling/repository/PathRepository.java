package net.thumbtack.school.carpooling.repository;

import net.thumbtack.school.carpooling.model.Path;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository interface for Path entity
 */
@Repository("pathRepository")
public interface PathRepository extends CrudRepository<Path, Long> {
    Path save(Path path);

    @Query(value = "SELECT ST_AsText(ST_Transform(b.way, 4326)) AS geometry from pgr_dijkstra (" +
            "    'SELECT osm_id as id, source, target, length as cost, reverse_cost FROM planet_osm_roads'," +
            "     :start, :end,true)" +
            "AS a" +
            " LEFT JOIN planet_osm_roads as b" +
            " ON (a.edge = b.osm_id) ORDER BY seq",
            nativeQuery = true)
    List<String> findShortPath(@Param("start") int start, @Param("end") int end);

    @Query(value = "SELECT id from planet_osm_roads_vertices_pgr where REPLACE(ST_AsText(geom_4326),'POINT','POINT ') = :point",
            nativeQuery = true)
    Integer getNode(@Param("point") String point);

    @Query(
            value = "SELECT * FROM path WHERE (ST_DWithin(ST_GeogFromText(:point),ST_GeogFromText(way),:r))",
            nativeQuery = true
    )
    List<Path> findWithFilter(@Param("point") String point,@Param("r") double radis);

    @Query(
            value = "Delete from path WHERE rideid = :rideId))",
            nativeQuery = true
    )
    void deleteByRideId(@Param("rideId")long rideId);

    void deleteAll();


}
