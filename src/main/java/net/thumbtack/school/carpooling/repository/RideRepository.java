package net.thumbtack.school.carpooling.repository;

import net.thumbtack.school.carpooling.model.Ride;
import net.thumbtack.school.carpooling.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository interface for Ride entity
 */
@Repository("rideRepository")
public interface RideRepository extends CrudRepository<Ride, Long> {
    Ride save(Ride ride);

    Ride findById(long id);

    void deleteById(long id);

    @Query(value = "select * from request where userid = :id",
            nativeQuery = true)
    List<Ride> findAll(@Param("id") long userId);

    void deleteAll();


}
