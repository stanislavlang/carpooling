package net.thumbtack.school.carpooling.repository;


import net.thumbtack.school.carpooling.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repotisory interface for User entity
 */
@Repository("userRepository")
public interface UserRepository extends CrudRepository<User, Long> {
     List<User> findAll();
     User save(User user);
     User findById(long id);
     @Query("select u from User u where u.email = :email")
     User findByEmail(@Param("email") String email);
     void deleteById(long id);
     void deleteAll();
}
