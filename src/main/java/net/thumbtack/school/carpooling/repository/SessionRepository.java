package net.thumbtack.school.carpooling.repository;

import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository interface for Session entity
 */
@Repository("sessionRepository")
public interface SessionRepository extends CrudRepository<Session,Long> {
    List<Session> findAll();
    Session save(Session session);
    Session findByToken(String token);
    void deleteById(long id);
    void deleteAll();
}
