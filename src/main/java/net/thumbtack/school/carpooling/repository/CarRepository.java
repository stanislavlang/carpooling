package net.thumbtack.school.carpooling.repository;

import net.thumbtack.school.carpooling.model.Car;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository interface for Car entity
 */
@Repository("carRepository")
public interface CarRepository extends CrudRepository<Car, Long> {
    Car save(Car car);

    Car findById(long id);

    void deleteById(long id);

    @Query(value = "select * from car where userid = :id",
            nativeQuery = true)
    List<Car> findAll(@Param("id")long id);

    void deleteAll();
}
