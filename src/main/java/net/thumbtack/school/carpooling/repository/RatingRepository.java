package net.thumbtack.school.carpooling.repository;

import net.thumbtack.school.carpooling.model.Rating;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository interface for Rating entity
 */
@Repository("ratingRepository")
public interface RatingRepository extends CrudRepository<Rating, Long> {
    Rating save(Rating rating);

    @Query("Select avg(r.rating) FROM Rating r where r.estimatedUser.id = :id")
    Integer getAvgRating(@Param("id") long userId);

    void deleteAll();

    void deleteById(long id);

    @Query(value = "select * from rating where userid = :id",
            nativeQuery = true)
    List<Rating> findAll(@Param("id")long id);
}
