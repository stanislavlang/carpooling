package net.thumbtack.school.carpooling.repository;

import net.thumbtack.school.carpooling.model.Request;
import net.thumbtack.school.carpooling.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository interface for Request entity
 */
@Repository("requestRepository")
public interface RequestRepository extends CrudRepository<Request,Long> {
    Request save(Request request);
    Request findById(long id);
    @Query(value = "select r from Request r where r.ride.id = :id")
    List<Request> findAll(@Param("id") long id);
    void deleteById(long id);
    void deleteAll();

    @Query("Select r.user FROM Request r where r.ride.id = :id and r.requestStatus = 'ACCEPT' ")
    List<User> getRideUsers(@Param("id") long rideId);
}
