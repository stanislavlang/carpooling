package net.thumbtack.school.carpooling.response;

/**
 * DTO response for @DeleteMapping(value = "/api/rate/{id}" in RatingController
 */
public class UserDeleteRateDtoResponse {
    private String status;

    public UserDeleteRateDtoResponse() {
    }

    public UserDeleteRateDtoResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
