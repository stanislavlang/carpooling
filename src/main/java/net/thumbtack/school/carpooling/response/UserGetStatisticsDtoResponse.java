package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.model.User;

/**
 * DTO response for api/user/statistics endpoint
 */
public class UserGetStatisticsDtoResponse {
    private Long id;
    private User user;
    private int ridesCount;

    public UserGetStatisticsDtoResponse() {
    }

    public UserGetStatisticsDtoResponse(Long id, User user, int ridesCount) {
        this.id = id;
        this.user = user;
        this.ridesCount = ridesCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getRidesCount() {
        return ridesCount;
    }

    public void setRidesCount(int ridesCount) {
        this.ridesCount = ridesCount;
    }
}
