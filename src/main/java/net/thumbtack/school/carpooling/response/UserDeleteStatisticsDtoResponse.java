package net.thumbtack.school.carpooling.response;

/**
 * DTO response for delete statistics endpoint
 */
public class UserDeleteStatisticsDtoResponse {
    private String status;

    public UserDeleteStatisticsDtoResponse() {

    }

    public UserDeleteStatisticsDtoResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
