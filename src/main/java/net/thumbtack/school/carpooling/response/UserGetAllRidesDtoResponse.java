package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.model.Ride;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO response for get /api/rides endpoint
 */
public class UserGetAllRidesDtoResponse {
    private List<Ride> rides = new ArrayList<>();

    public UserGetAllRidesDtoResponse() {
    }

    public UserGetAllRidesDtoResponse(List<Ride> rides) {
        this.rides = rides;
    }

    public List<Ride> getRides() {
        return rides;
    }

    public void setRides(List<Ride> rides) {
        this.rides = rides;
    }
}
