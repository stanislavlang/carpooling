package net.thumbtack.school.carpooling.response;

/**
 *  DTO response for /api/user/rate endpoint
 */
public class UserGetRatingDtoResponse {
    private long id;
    private String name;
    private String email;
    private int rating;

    public UserGetRatingDtoResponse() {
    }

    public UserGetRatingDtoResponse(long id, String name, String email, int rating) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
