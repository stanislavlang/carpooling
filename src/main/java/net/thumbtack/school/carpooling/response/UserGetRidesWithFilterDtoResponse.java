package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.model.Ride;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO response for  @GetMapping value = "/api/ride" RideController endpoint
 */
public class UserGetRidesWithFilterDtoResponse {
    private List<RideDtoResponse> rides;

    public UserGetRidesWithFilterDtoResponse() {
        rides = new ArrayList<>();
    }

    public UserGetRidesWithFilterDtoResponse(List<RideDtoResponse> rides) {
        this.rides = rides;
    }

    public List<RideDtoResponse> getRides() {
        return rides;
    }

    public void setRides(List<RideDtoResponse> rides) {
        this.rides = rides;
    }
}
