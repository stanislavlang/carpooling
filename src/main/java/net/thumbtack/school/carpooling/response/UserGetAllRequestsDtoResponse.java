package net.thumbtack.school.carpooling.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.03.2019.
 */
public class UserGetAllRequestsDtoResponse {
    private List<RequestDtoResponse> requests = new ArrayList<>();

    public UserGetAllRequestsDtoResponse() {
    }

    public UserGetAllRequestsDtoResponse(List<RequestDtoResponse> requests) {
        this.requests = requests;
    }

    public List<RequestDtoResponse> getRequests() {
        return requests;
    }

    public void setRequests(List<RequestDtoResponse> requests) {
        this.requests = requests;
    }
}
