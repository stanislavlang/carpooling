package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.enums.RequestStatus;

/**
 * Created by RATpr on 21.03.2019.
 */
public class RequestDtoResponse {
    private long id;
    private String name;
    private String email;
    private RequestStatus requestStatus;

    public RequestDtoResponse() {
    }

    public RequestDtoResponse(long id, String name, String email, RequestStatus requestStatus) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.requestStatus = requestStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }
}
