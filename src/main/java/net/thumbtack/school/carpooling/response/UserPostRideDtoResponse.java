package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.enums.RideStatus;
import net.thumbtack.school.carpooling.model.Car;
import net.thumbtack.school.carpooling.model.Path;
import net.thumbtack.school.carpooling.request.PointDto;

import java.util.Date;
import java.util.List;

/**
 * DTO response for /api/ride endpoint
 */
public class UserPostRideDtoResponse {
    private long id;
    private String departureTime;
    private int seatsCount;
    private String carBrand;
    private String carColor;
    private String carLicensePlate;
    private String carType;
    private RideStatus rideStatus;
    private List<List> path;

    public UserPostRideDtoResponse() {
    }

    public UserPostRideDtoResponse(long id, String departureTime, int seatsCount, String carBrand, String carColor,
                                        String carLicensePlate, String carType, List<List> path, RideStatus rideStatus) {
        this.id = id;
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
        this.carBrand = carBrand;
        this.carColor = carColor;
        this.carLicensePlate = carLicensePlate;
        this.carType = carType;
        this.rideStatus = rideStatus;
        this.path = path;

    }

    public RideStatus getRideStatus() {
        return rideStatus;
    }

    public void setRideStatus(RideStatus rideStatus) {
        this.rideStatus = rideStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public int getSeatsCount() {
        return seatsCount;
    }

    public void setSeatsCount(int seatsCount) {
        this.seatsCount = seatsCount;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getCarLicensePlate() {
        return carLicensePlate;
    }

    public void setCarLicensePlate(String carLicensePlate) {
        this.carLicensePlate = carLicensePlate;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public List<List> getPath() {
        return path;
    }

    public void setPath(List<List> path) {
        this.path = path;
    }
}
