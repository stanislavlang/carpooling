package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.model.Rating;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.03.2019.
 */
public class UserGetAllRatesDtoResponse {
    List<Rating> rates = new ArrayList<>();

    public UserGetAllRatesDtoResponse() {
    }

    public UserGetAllRatesDtoResponse(List<Rating> rates) {
        this.rates = rates;
    }

    public List<Rating> getRates() {
        return rates;
    }

    public void setRates(List<Rating> rates) {
        this.rates = rates;
    }
}
