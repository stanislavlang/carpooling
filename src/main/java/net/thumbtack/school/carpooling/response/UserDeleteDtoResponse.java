package net.thumbtack.school.carpooling.response;

/**
 * DTO response for @DeleteMapping(value = "/api/user/{id}" in UserController
 */
public class UserDeleteDtoResponse {
    private String status;

    public UserDeleteDtoResponse() {
    }

    public UserDeleteDtoResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
