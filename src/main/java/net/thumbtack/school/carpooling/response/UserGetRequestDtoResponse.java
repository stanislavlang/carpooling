package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.enums.RequestStatus;

/**
 * DTO response for  @GetMapping(value = "/api/request/{id}" RequestController
 */
public class UserGetRequestDtoResponse {
    private long id;
    private RequestStatus requestStatus;

    public UserGetRequestDtoResponse() {
    }

    public UserGetRequestDtoResponse(long id, RequestStatus requestStatus) {
        this.id = id;
        this.requestStatus = requestStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }
}
