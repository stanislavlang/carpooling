package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.model.Path;

import java.util.Date;
import java.util.List;

/**
 * DTO response for update /api/ride/{id}/path endpoint
 */
public class UserUpdateRidePathDtoResponse {
    private long rideId;
     private List<Path> path;

    public UserUpdateRidePathDtoResponse() {
    }

    public UserUpdateRidePathDtoResponse(long rideId, List<Path> path) {
        this.rideId = rideId;
        this.path = path;
    }

    public long getRideId() {
        return rideId;
    }

    public void setRideId(long rideId) {
        this.rideId = rideId;
    }

    public List<Path> getPath() {
        return path;
    }

    public void setPath(List<Path> path) {
        this.path = path;
    }
}
