package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.enums.RideStatus;

import java.util.Date;

/**
 * DTO response for update /api/ride endpoint
 */
public class UserFinishRideDtoResponse {
    private long id;
    private Date departureTime;
    private int seatsCount;
    private String carBrand;
    private String carColor;
    private String carLicensePlate;
    private String carType;
    private RideStatus rideStatus;

    public UserFinishRideDtoResponse() {
    }

    public UserFinishRideDtoResponse(long id, Date departureTime, int seatsCount, String carBrand, String carColor,
                                     String carLicensePlate, String carType, RideStatus rideStatus) {
        this.id = id;
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
        this.carBrand = carBrand;
        this.carColor = carColor;
        this.carLicensePlate = carLicensePlate;
        this.carType = carType;
        this.rideStatus = rideStatus;
    }

    public RideStatus getRideStatus() {
        return rideStatus;
    }

    public void setRideStatus(RideStatus rideStatus) {
        this.rideStatus = rideStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public int getSeatsCount() {
        return seatsCount;
    }

    public void setSeatsCount(int seatsCount) {
        this.seatsCount = seatsCount;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getCarLicensePlate() {
        return carLicensePlate;
    }

    public void setCarLicensePlate(String carLicensePlate) {
        this.carLicensePlate = carLicensePlate;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }
}
