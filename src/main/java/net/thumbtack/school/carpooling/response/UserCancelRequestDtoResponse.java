package net.thumbtack.school.carpooling.response;

/**
 * Created by User on 04.03.2019.
 */
public class UserCancelRequestDtoResponse {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserCancelRequestDtoResponse() {
    }

    public UserCancelRequestDtoResponse(String status) {
        this.status = status;
    }
}
