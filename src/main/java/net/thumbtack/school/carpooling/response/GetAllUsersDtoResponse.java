package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO response for get /api/users endpoint
 */
public class GetAllUsersDtoResponse {
    List<User> users = new ArrayList<>();

    public GetAllUsersDtoResponse() {
    }

    public GetAllUsersDtoResponse(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
