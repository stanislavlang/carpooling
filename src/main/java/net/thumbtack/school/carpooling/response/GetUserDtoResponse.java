package net.thumbtack.school.carpooling.response;

/**
 * DTO response for get /api/user endpoint
 */
public class GetUserDtoResponse {
    private Long id;
    private String email;
    private int rating;
    private String name;

    public GetUserDtoResponse() {
    }

    public GetUserDtoResponse(long id, String email, int rating) {
        this.id = id;
        this.email = email;
        this.rating = rating;
    }

    public GetUserDtoResponse(Long id, String email, int rating, String name) {
        this.id = id;
        this.email = email;
        this.rating = rating;
        this.name = name;
    }

    public GetUserDtoResponse(Long id, String email, String name) {
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
