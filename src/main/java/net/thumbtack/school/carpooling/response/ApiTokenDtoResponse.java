package net.thumbtack.school.carpooling.response;

/**
 * DTO response for /auth endpoint
 */
public class ApiTokenDtoResponse {
    private String token;

    public ApiTokenDtoResponse() {
    }

    public ApiTokenDtoResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
