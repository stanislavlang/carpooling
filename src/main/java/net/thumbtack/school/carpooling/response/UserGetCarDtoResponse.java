package net.thumbtack.school.carpooling.response;

/**
 * DTO response for  @GetMapping(value = "/api/car/{carId}" endpoint
 */
public class UserGetCarDtoResponse {
    private Long id;
    private String brand;
    private String color;
    private String type;
    private String licensePlate;

    public UserGetCarDtoResponse() {
    }

    public UserGetCarDtoResponse(Long id, String brand, String color, String type, String licensePlate) {
        this.id = id;
        this.brand = brand;
        this.color = color;
        this.type = type;
        this.licensePlate = licensePlate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }
}
