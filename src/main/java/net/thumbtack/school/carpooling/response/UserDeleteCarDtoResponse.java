package net.thumbtack.school.carpooling.response;

/**
 * DTO response for @DeleteMapping(value = "/api/car/{carId}" in CarController
 */
public class UserDeleteCarDtoResponse {
    private String status;

    public UserDeleteCarDtoResponse() {
    }

    public UserDeleteCarDtoResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
