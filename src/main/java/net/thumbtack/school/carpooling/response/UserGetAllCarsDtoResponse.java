package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.model.Car;

import java.util.List;

/**
 * Created by User on 12.03.2019.
 */
public class UserGetAllCarsDtoResponse {
    private List<Car> cars;

    public UserGetAllCarsDtoResponse() {
    }

    public UserGetAllCarsDtoResponse(List<Car> cars) {
        this.cars = cars;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
