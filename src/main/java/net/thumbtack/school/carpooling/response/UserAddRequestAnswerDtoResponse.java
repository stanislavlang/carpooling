package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.enums.RequestStatus;
import net.thumbtack.school.carpooling.model.Request;

/**
 * Created by User on 04.03.2019.
 */
public class UserAddRequestAnswerDtoResponse {
    private long requestId;
    private RequestStatus request;


    public UserAddRequestAnswerDtoResponse() {
    }

    public UserAddRequestAnswerDtoResponse(long requestId, RequestStatus request) {
        this.requestId = requestId;
        this.request = request;
    }

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public RequestStatus getRequest() {
        return request;
    }

    public void setRequest(RequestStatus request) {
        this.request = request;
    }
}
