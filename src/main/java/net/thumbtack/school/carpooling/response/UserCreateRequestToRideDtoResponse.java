package net.thumbtack.school.carpooling.response;

import net.thumbtack.school.carpooling.enums.RequestStatus;

/**
 * DTO response for /api/ride/{id} endpoint
 */
public class UserCreateRequestToRideDtoResponse {
    private long id;
    private RequestStatus requestStatus;

    public UserCreateRequestToRideDtoResponse() {
    }

    public UserCreateRequestToRideDtoResponse(long id, RequestStatus requestStatus) {
        this.id = id;
        this.requestStatus = requestStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }
}
