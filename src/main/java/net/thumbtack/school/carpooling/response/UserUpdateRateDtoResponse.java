package net.thumbtack.school.carpooling.response;

/**
 * DTO response for @PutMapping(value = "/api/user/{id}/rate" rate endpoint
 */
public class UserUpdateRateDtoResponse {
    private Long id;
    private int rating;

    public UserUpdateRateDtoResponse() {
    }

    public UserUpdateRateDtoResponse(Long id, int rating) {
        this.id = id;
        this.rating = rating;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
