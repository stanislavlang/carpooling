package net.thumbtack.school.carpooling.response;

import java.time.LocalDateTime;
import java.util.Date;


/**
 * DTO response for get /api/ride endpoint
 */
public class UserGetRideDtoResponse {
    private long id;
    private Date departureTime;
    private int seatsCount;
    private String carBrand;
    private String carColor;
    private String carLicensePlate;
    private String carType;

    public UserGetRideDtoResponse() {
    }

    public UserGetRideDtoResponse(long id, Date departureTime, int seatsCount,String carBrand, String carColor, String carLicensePlate, String carType) {
        this.id = id;
        this.departureTime = departureTime;
        this.seatsCount = seatsCount;
        this.carBrand = carBrand;
        this.carColor = carColor;
        this.carLicensePlate = carLicensePlate;
        this.carType = carType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public int getSeatsCount() {
        return seatsCount;
    }

    public void setSeatsCount(int seatsCount) {
        this.seatsCount = seatsCount;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getCarLicensePlate() {
        return carLicensePlate;
    }

    public void setCarLicensePlate(String carLicensePlate) {
        this.carLicensePlate = carLicensePlate;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }
}
