package net.thumbtack.school.carpooling.controller;

import net.thumbtack.school.carpooling.response.UserDeleteStatisticsDtoResponse;
import net.thumbtack.school.carpooling.response.UserGetStatisticsDtoResponse;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import net.thumbtack.school.carpooling.service.StatisticsService;
import net.thumbtack.school.carpooling.service.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller implements functional for Statistics entity
 */
@RestController("statisticsController")
public class StatisticsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsController.class);
    @Autowired
    private StatisticsService statisticsService;
    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @GetMapping(value = "/api/statistics", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetStatisticsDtoResponse getStatistics(HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return statisticsService.getStatistics(userToken);
    }
    @DeleteMapping(value = "/api/statistics", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserDeleteStatisticsDtoResponse deleteStatistics(HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return statisticsService.deleteStatistics(userToken);
    }

}
