package net.thumbtack.school.carpooling.controller;

import net.thumbtack.school.carpooling.request.UserAddRequestAnswerDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateRequestDtoRequest;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import net.thumbtack.school.carpooling.service.RequestService;
import net.thumbtack.school.carpooling.service.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Controller implements functional for Request entity
 */
@RestController("requestController")
public class RequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestController.class);
    @Autowired
    private RequestService requestService;
    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @PostMapping(value = "/api/ride/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserCreateRequestToRideDtoResponse createRequest(HttpServletRequest httpServletRequest,
                                                            @PathVariable("id") long rideId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return requestService.createRequest(userToken, rideId);
    }

    @PostMapping(value = "/api/requests/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserAddRequestAnswerDtoResponse addRequestAnswer(HttpServletRequest httpServletRequest,
                                                            @PathVariable("id") long requestId,
                                                            @Valid @RequestBody UserAddRequestAnswerDtoRequest userAddRequestAnswerDtoRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return requestService.addRequestAnswer(userToken, requestId, userAddRequestAnswerDtoRequest);
    }

    @PutMapping(value = "/api/requests/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserUpdateRequestDtoResponse updateRequest(HttpServletRequest httpServletRequest,
                                                      @PathVariable("id") long requestId,
                                                      @Valid @RequestBody UserUpdateRequestDtoRequest userUpdateRequestDtoRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return requestService.updateRequest(userToken, requestId, userUpdateRequestDtoRequest);
    }

    @DeleteMapping(value = "/api/requests/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserCancelRequestDtoResponse cancelRequest(HttpServletRequest httpServletRequest,
                                                      @PathVariable("id") long requestId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return requestService.cancelRequest(userToken, requestId);
    }

    @GetMapping(value = "/api/requests/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetRequestDtoResponse getRequest(HttpServletRequest httpServletRequest,
                                                @PathVariable("id") long requestId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return requestService.getRequest(userToken, requestId);
    }

    @GetMapping(value = "/api/rides/{id}/requests", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetAllRequestsDtoResponse getAllRequests(HttpServletRequest httpServletRequest,
                                                        @PathVariable("id") long rideId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return requestService.getAllRequests(userToken,rideId);
    }

}
