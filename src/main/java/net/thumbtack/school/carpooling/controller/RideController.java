package net.thumbtack.school.carpooling.controller;

import net.thumbtack.school.carpooling.request.UserGetRidesWithFilterDtoRequest;
import net.thumbtack.school.carpooling.request.UserPostRideDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateRideDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateRidePathDtoRequest;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import net.thumbtack.school.carpooling.service.RideService;
import net.thumbtack.school.carpooling.service.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Controller implements functional for Ride entity
 */
@RestController("rideController")
public class RideController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RideController.class);
    @Autowired
    private RideService rideService;
    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @PostMapping(value = "/api/ride", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserPostRideDtoResponse postRide(@Valid @RequestBody UserPostRideDtoRequest userDto,
                                            HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return rideService.postRide(userDto, userToken);
    }

    @PutMapping(value = "/api/rides/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserUpdateRideDtoResponse updateRide(@Valid @RequestBody UserUpdateRideDtoRequest userUpdateRideDtoRequest,
                                                HttpServletRequest httpServletRequest,
                                                @PathVariable("id") long rideId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return rideService.updateRide(userUpdateRideDtoRequest, userToken, rideId);
    }

    @PutMapping(value = "/api/rides/{id}/path", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserUpdateRidePathDtoResponse updateRidePath(@Valid @RequestBody UserUpdateRidePathDtoRequest userUpdateRidePathDtoRequest,
                                                HttpServletRequest httpServletRequest,
                                                @PathVariable("id") long rideId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return rideService.updateRidePath(userUpdateRidePathDtoRequest, userToken, rideId);
    }

    @DeleteMapping(value = "/api/rides/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserCancelRideDtoResponse cancelRide(HttpServletRequest httpServletRequest,
                                                @PathVariable("id") long rideId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return rideService.cancelRide(userToken, rideId);
    }

    @GetMapping(value = "/api/rides/filter", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetRidesWithFilterDtoResponse getRidesWithFilter(HttpServletRequest httpServletRequest,
                                                                @Valid @RequestBody UserGetRidesWithFilterDtoRequest userGetRidesWithFilterDtoRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return rideService.getRidesWithFilter(userToken, userGetRidesWithFilterDtoRequest);
    }

    @GetMapping(value = "/api/rides/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetRideDtoResponse getRide(HttpServletRequest httpServletRequest,
                                          @PathVariable("id") long rideId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return rideService.getRide(userToken, rideId);
    }

    @GetMapping(value = "/api/rides", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetAllRidesDtoResponse getAllRides(HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return rideService.getAllRides(userToken);
    }


}
