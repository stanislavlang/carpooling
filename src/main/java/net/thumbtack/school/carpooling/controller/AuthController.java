package net.thumbtack.school.carpooling.controller;

import net.thumbtack.school.carpooling.enums.ServerErrorCode;
import net.thumbtack.school.carpooling.request.UserLoginDtoRequest;
import net.thumbtack.school.carpooling.response.ApiTokenDtoResponse;
import net.thumbtack.school.carpooling.response.UserLoginDtoResponse;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import net.thumbtack.school.carpooling.service.TokenAuthenticationService;
import net.thumbtack.school.carpooling.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Controller takes google token, generates api token and retrieves information of user.
 */
@RestController("authController")
public class AuthController {
    @Autowired
    private UserService userService;
    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;
    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;
    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    @GetMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ApiTokenDtoResponse getLoginInfo(Model model, OAuth2AuthenticationToken authentication,
                                            HttpServletResponse httpServletResponse) throws CarpoolingException {

        OAuth2AuthorizedClient client = authorizedClientService.loadAuthorizedClient(authentication.getAuthorizedClientRegistrationId(), authentication.getName());
        String userInfoEndpointUri = client.getClientRegistration()
                .getProviderDetails()
                .getUserInfoEndpoint()
                .getUri();

        if (StringUtils.isEmpty(userInfoEndpointUri)) {
            throw new CarpoolingException(ServerErrorCode.NULL_USER, "User");
        } else {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + client.getAccessToken()
                    .getTokenValue());
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            ResponseEntity<Map> response = restTemplate.exchange(userInfoEndpointUri, HttpMethod.GET, entity, Map.class);
            Map userAttributes = response.getBody();
            model.addAttribute("name", userAttributes.get("name"));
            UserLoginDtoRequest userLoginDtoRequest = new UserLoginDtoRequest((String) (userAttributes.get("email")), (String) (userAttributes.get("name")));
            UserLoginDtoResponse userLoginDtoResponse = userService.saveUser(userLoginDtoRequest);
            String token = tokenAuthenticationService.addAuthentication(httpServletResponse, userLoginDtoResponse);
            return new ApiTokenDtoResponse(token);
        }
    }

}
