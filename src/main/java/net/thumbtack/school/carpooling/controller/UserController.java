package net.thumbtack.school.carpooling.controller;


import net.thumbtack.school.carpooling.request.UserLoginDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateDtoRequest;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import net.thumbtack.school.carpooling.service.TokenAuthenticationService;
import net.thumbtack.school.carpooling.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

/**
 * Controller implements user functional
 */

@RestController("userController")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @PostMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserLoginDtoResponse createUser(@Valid @RequestBody UserLoginDtoRequest userDto) {
        return userService.saveUser(userDto);
    }

    @PutMapping(value = "/api/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserUpdateDtoResponse updateUser(HttpServletRequest httpServletRequest,
                                            @Valid @RequestBody UserUpdateDtoRequest userDto,
                                            @PathVariable("id") long rideId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return userService.updateUser(userToken, userDto);
    }

    @DeleteMapping(value = "/api/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDeleteDtoResponse deleteUser(HttpServletRequest httpServletRequest,
                                            @PathVariable("id") long userId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return userService.deleteUser(userToken, userId);
    }

    @GetMapping(value = "/api/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public GetUserDtoResponse getUser(HttpServletRequest httpServletRequest,
                                      @PathVariable("id") long userId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return userService.getUser(userToken, userId);
    }

    @GetMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public GetUserDtoResponse getOwnInfo(HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return userService.getOwnInfo(userToken);
    }

    @GetMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public GetAllUsersDtoResponse getAllUsers(HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return userService.getAllUsers(userToken);
    }



}
