package net.thumbtack.school.carpooling.controller;

import net.thumbtack.school.carpooling.request.UserAddRateDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateRateDtoRequest;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import net.thumbtack.school.carpooling.service.RatingService;
import net.thumbtack.school.carpooling.service.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Controller implements functional for Rating entity
 */
@RestController("ratingController")
public class RatingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RatingController.class);
    @Autowired
    private RatingService ratingService;
    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @PostMapping(value = "/api/rides/{rideId}/users/{userId}/rate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserAddRateDtoResponse addRate(@Valid @RequestBody UserAddRateDtoRequest userAddRateDtoRequest,
                                          HttpServletRequest httpServletRequest,
                                          @PathVariable("userId") long userId,
                                          @PathVariable("rideId") long rideId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return ratingService.addRate(userAddRateDtoRequest, userToken, userId,rideId);
    }

    @PutMapping(value = "/api/rates/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserUpdateRateDtoResponse updateRate(@Valid @RequestBody UserUpdateRateDtoRequest userUpdateRateDtoRequest,
                                                HttpServletRequest httpServletRequest,
                                                @PathVariable("id") long rateId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return ratingService.updateRate(userUpdateRateDtoRequest, userToken, rateId);
    }

    @GetMapping(value = "/api/user/rate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetRatingDtoResponse getRate(HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return ratingService.getRate(userToken);
    }

    @GetMapping(value = "/api/user/rates", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetAllRatesDtoResponse getAllRates(HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return ratingService.getAllRates(userToken);
    }

    @DeleteMapping(value = "/api/rates/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserDeleteRateDtoResponse deleteRate(HttpServletRequest httpServletRequest,
                                                @PathVariable("id") long rateId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return ratingService.deleteRate(userToken, rateId);
    }


}
