package net.thumbtack.school.carpooling.controller;

import net.thumbtack.school.carpooling.request.UserAddCarDtoRequest;
import net.thumbtack.school.carpooling.request.UserUpdateCarDtoRequest;
import net.thumbtack.school.carpooling.response.*;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import net.thumbtack.school.carpooling.service.CarService;
import net.thumbtack.school.carpooling.service.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Controller implements functional for Car entity
 */
@RestController("carController")
public class CarController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarController.class);
    @Autowired
    private CarService carService;
    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @PostMapping(value = "/api/car", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserAddCarDtoResponse addCar(@Valid @RequestBody UserAddCarDtoRequest userDto,
                                        HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return carService.addCar(userDto, userToken);
    }


    @PutMapping(value = "/api/cars/{carId}/approve", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ModeratorApproveCarDtoResponse approveCar(HttpServletRequest httpServletRequest,
                                                     @PathVariable("carId") long carId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return carService.approveCar(userToken, carId);
    }

    @PutMapping(value = "/api/cars/{carId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserUpdateCarDtoResponse updateCar(HttpServletRequest httpServletRequest,
                                              @Valid @RequestBody UserUpdateCarDtoRequest userUpdateCarDtoRequest,
                                              @PathVariable("carId") long carId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return carService.updateCar(userToken, carId, userUpdateCarDtoRequest);
    }

    @DeleteMapping(value = "/api/cars/{carId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserDeleteCarDtoResponse deleteCar(HttpServletRequest httpServletRequest,
                                              @PathVariable("carId") long carId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return carService.deleteCar(userToken, carId);
    }

    @GetMapping(value = "/api/cars/{carId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetCarDtoResponse getCar(HttpServletRequest httpServletRequest,
                                        @PathVariable("carId") long carId) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return carService.getCar(userToken, carId);
    }

    @GetMapping(value = "/api/cars", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserGetAllCarsDtoResponse getCars(HttpServletRequest httpServletRequest) throws CarpoolingException {
        String userToken = tokenAuthenticationService.getToken(httpServletRequest);
        return carService.getCars(userToken);
    }

}
