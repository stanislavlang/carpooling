package net.thumbtack.school.carpooling.controller;

import net.thumbtack.school.carpooling.enums.ServerErrorCode;
import net.thumbtack.school.carpooling.request.UserLoginDtoRequest;
import net.thumbtack.school.carpooling.response.ApiTokenDtoResponse;
import net.thumbtack.school.carpooling.response.UserLoginDtoResponse;
import net.thumbtack.school.carpooling.serverexception.CarpoolingException;
import net.thumbtack.school.carpooling.service.TokenAuthenticationService;
import net.thumbtack.school.carpooling.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Controller for google login
 */
@Controller
public class LoginController {

    @RequestMapping("/")
    public String loginPage() {
        return "index";
    }
}
