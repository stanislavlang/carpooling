package net.thumbtack.school.carpooling.carpooling.database;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;
import net.thumbtack.school.carpooling.model.Car;
import net.thumbtack.school.carpooling.model.Path;
import net.thumbtack.school.carpooling.model.Ride;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.List;

/**
 * Class for testing Ride table in Database
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RideRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private PathRepository pathRepository;

    @Before
    public void init() {
        requestRepository.deleteAll();
        pathRepository.deleteAll();
        rideRepository.deleteAll();
    }

    @Test
    public void userCreateRide() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Calendar cal = Calendar.getInstance();
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);
        carRepository.save(car);
        GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);
        Ride ride = new Ride(cal.getTime(), 3);
        ride.setCar(car);
        ride.setUser(user);

        rideRepository.save(ride);
        Assert.assertNotNull(ride.getId());
    }

    @Test
    public void userFindShortPath() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Calendar cal = Calendar.getInstance();
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);
        carRepository.save(car);
        GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);


        Ride ride = new Ride(cal.getTime(), 3);
        ride.setCar(car);
        ride.setUser(user);
        rideRepository.save(ride);
        Point start = gf.createPoint(new Coordinate(40.3434934, 44.5829085005027));
        Point end = gf.createPoint(new Coordinate(40.5838854, 44.3899401005149));
        List<String> paths = pathRepository.findShortPath(pathRepository.getNode(start.toString()),
                pathRepository.getNode(end.toString()));
        Assert.assertNotNull(paths.size());
    }

    @Test
    public void userGetRideWithFilter() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Calendar cal = Calendar.getInstance();
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);
        carRepository.save(car);
        GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);
        Ride ride = new Ride(cal.getTime(), 3);
        ride.setCar(car);
        ride.setUser(user);
        rideRepository.save(ride);
        Point start = gf.createPoint(new Coordinate(40.3434934, 44.5829085005027));
        Point end = gf.createPoint(new Coordinate(40.5838854, 44.3899401005149));
        List<String> paths = pathRepository.findShortPath(pathRepository.getNode(start.toString()),
                pathRepository.getNode(end.toString()));
        int pathPart = 0;
        while (pathPart < paths.size()) {
            Path path = new Path(paths.get(pathPart), pathPart);
            path.setRide(ride);
            pathRepository.save(path);
            pathPart++;
        }
        List<Path> startPath = pathRepository.findWithFilter(start.toString(), 100);
        List<Path> endPath = pathRepository.findWithFilter(end.toString(), 100);

    }


}
