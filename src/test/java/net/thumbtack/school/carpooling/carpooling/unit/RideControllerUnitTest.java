package net.thumbtack.school.carpooling.carpooling.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.carpooling.controller.RideController;
import net.thumbtack.school.carpooling.enums.RideStatus;
import net.thumbtack.school.carpooling.request.UserPostRideDtoRequest;
import net.thumbtack.school.carpooling.response.UserPostRideDtoResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Class for unit testing RideController endpoint
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = RideController.class, secure = false)
public class RideControllerUnitTest {
    /*
    @MockBean
    private RideController rideController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void testUserPostRide() throws Exception {
        UserPostRideDtoRequest userPostRideDtoRequest = new UserPostRideDtoRequest(new Date(), 5, 40, 40, 40, 40, 1);
        UserPostRideDtoResponse userPostRideDtoResponse = new UserPostRideDtoResponse(new Long(1), new Date().toString(), 5,
                "Honda", "red", "ЕК777Х55", "sedan",new ArrayList<>(), RideStatus.WAITING);
        Mockito.when(rideController.postRide(any(UserPostRideDtoRequest.class), any(HttpServletRequest.class))).thenReturn(userPostRideDtoResponse);

        MvcResult result = this.mvc.perform(
                post("/api/ride")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(userPostRideDtoRequest)))
                .andReturn();
        UserPostRideDtoResponse userPostRideDtoResponse2 = mapper.readValue(result.getResponse().getContentAsString(), UserPostRideDtoResponse.class);
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertNotNull(userPostRideDtoResponse2.getId());
    }


    @Test
    public void testCancelRide() throws Exception {
        MvcResult result = this.mvc.perform(
                delete("/api/rides/{id}", 100)
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(null)))
                .andReturn();
        Assert.assertEquals(200, result.getResponse().getStatus());
    }
    */

}
