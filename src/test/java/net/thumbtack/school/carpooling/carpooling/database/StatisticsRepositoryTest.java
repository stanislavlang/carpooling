package net.thumbtack.school.carpooling.carpooling.database;

import net.thumbtack.school.carpooling.model.Statistics;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.StatisticsRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class for testing Statistics table in Database
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StatisticsRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StatisticsRepository statisticsRepository;

    @Test
    public void saveStatistic() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Statistics statistics = new Statistics(2);
        statistics.setUser(user);

        statisticsRepository.save(statistics);
        Assert.assertNotNull(statistics.getId());
    }

    @Test
    public void getStatisticsByUserId() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Statistics statistics = new Statistics(2);
        statistics.setUser(user);
        statisticsRepository.save(statistics);

        Statistics statistics2 = statisticsRepository.getStatisticsByUserId(user.getId());
        Assert.assertEquals(statistics2.getRidesCount(),statistics.getRidesCount());
    }
}
