package net.thumbtack.school.carpooling.carpooling.database;

import net.thumbtack.school.carpooling.model.Rating;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.RatingRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class for testing Rating table in Database
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RatingRepositoryTest {
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private UserRepository userRepository;

    @Before
    public void init() {
        ratingRepository.deleteAll();
    }

    @Test
    public void getAvgRate() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        User user2 = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Rating rating = new Rating(5);
        rating.setEstimatedUser(user);
        rating.setEvaluatingUser(user2);
        ratingRepository.save(rating);
        Integer avgRating = ratingRepository.getAvgRating(user.getId());
        Assert.assertNotNull(avgRating);
    }
}
