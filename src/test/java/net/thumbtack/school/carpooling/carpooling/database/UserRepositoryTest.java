package net.thumbtack.school.carpooling.carpooling.database;

import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.SessionRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class for testing User table in Database
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;

    @Test
    public void addUser() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Assert.assertNotNull(user.getId());
    }

    @Test
    public void getUser() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));

        User user2 = userRepository.findById(user.getId()).get();
        Assert.assertEquals(user.getName(), user2.getName());
        Assert.assertEquals(user.getEmail(), user2.getEmail());
        Assert.assertEquals(user.getId(), user2.getId());
    }

    @Test
    public void getUserBySession() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Session session = new Session("session34");
        session.setUser(user);
        sessionRepository.save(session);
        Session session2 = sessionRepository.findByToken("session34");
        User user2 = userRepository.findById(session.getUser().getId()).orElseThrow(() -> new NullPointerException());
        Assert.assertNotNull(user2.getId());
        Assert.assertEquals(user.getName(), user2.getName());
        Assert.assertEquals(user.getEmail(), user2.getEmail());
        Assert.assertEquals(user.getId(), user2.getId());
    }
}
