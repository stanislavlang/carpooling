package net.thumbtack.school.carpooling.carpooling.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.carpooling.controller.CarController;
import net.thumbtack.school.carpooling.request.UserAddCarDtoRequest;
import net.thumbtack.school.carpooling.response.UserAddCarDtoResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Class for unit testing CarController endpoint
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = CarController.class, secure = false)
public class CarControllerUnitTest {
    @MockBean
    private CarController carController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void testUserAddCar() throws Exception {
        UserAddCarDtoRequest userAddCarDtoRequest = new UserAddCarDtoRequest("Honda Accord", "red", "sedan", "Е666КХ");
        UserAddCarDtoResponse userAddCarDtoResponse = new UserAddCarDtoResponse(new Long(1), "Honda Accord", "red", "sedan", "Е666КХ");
        Mockito.when(carController.addCar(any(UserAddCarDtoRequest.class), any(HttpServletRequest.class))).thenReturn(userAddCarDtoResponse);

        MvcResult result = this.mvc.perform(
                post("/api/car")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(userAddCarDtoRequest)))
                .andReturn();
        UserAddCarDtoResponse userAddCarDtoResponse2 = mapper.readValue(result.getResponse().getContentAsString(), UserAddCarDtoResponse.class);
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertNotNull(userAddCarDtoResponse2.getId());
        Assert.assertEquals(userAddCarDtoRequest.getBrand(), userAddCarDtoResponse2.getBrand());
        Assert.assertEquals(userAddCarDtoRequest.getColor(), userAddCarDtoResponse2.getColor());
        Assert.assertEquals(userAddCarDtoRequest.getLicensePlate(), userAddCarDtoResponse2.getLicensePlate());
        Assert.assertEquals(userAddCarDtoRequest.getType(), userAddCarDtoResponse2.getType());
    }

}
