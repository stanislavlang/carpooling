package net.thumbtack.school.carpooling.carpooling.resttemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.PrecisionModel;
import net.thumbtack.school.carpooling.enums.RequestStatus;
import net.thumbtack.school.carpooling.model.*;
import net.thumbtack.school.carpooling.repository.*;
import net.thumbtack.school.carpooling.request.UserAddRequestAnswerDtoRequest;
import net.thumbtack.school.carpooling.response.UserAddRequestAnswerDtoResponse;
import net.thumbtack.school.carpooling.response.UserCancelRequestDtoResponse;
import net.thumbtack.school.carpooling.response.UserCreateRequestToRideDtoResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for integration testing UserController endpoint
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RequestControllerRestTemplateTest {
/*
    @LocalServerPort
    private int port;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private StatisticsRepository statisticsRepository;
    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private PathRepository pathRepository;

    @Autowired
    private ObjectMapper mapper;

    @Before
    public void init() {
        sessionRepository.deleteAll();
        requestRepository.deleteAll();
        pathRepository.deleteAll();
        rideRepository.deleteAll();
        ratingRepository.deleteAll();
        statisticsRepository.deleteAll();
        carRepository.deleteAll();
        userRepository.deleteAll();

    }


    @Test
    public void testCreateRequest() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        User userForRequest = userRepository.save(new User("petrov62@gmail.com", "Петров Иван"));
        Session session = new Session("session");
        session.setUser(userForRequest);
        sessionRepository.save(session);
        Calendar cal = Calendar.getInstance();
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);
        carRepository.save(car);
        Ride ride = new Ride(cal.getTime(), 3);
        ride.setCar(car);
        ride.setUser(user);
        rideRepository.save(ride);
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        String url = "http://localhost:" + port + "/api/ride/{id}";
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Access-Token", session.getToken());
        Map<String, Long> params = new HashMap<>();
        params.put("id", ride.getId());
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<UserCreateRequestToRideDtoResponse> result = restTemplate.exchange(builder.buildAndExpand(params).toUri(),
                HttpMethod.POST, entity, UserCreateRequestToRideDtoResponse.class);
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertNotNull(result.getBody().getId());
    }

    @Test
    public void testAddRequestAnswer() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Session session = new Session("session");
        session.setUser(user);
        sessionRepository.save(session);
        Calendar cal = Calendar.getInstance();
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);
        carRepository.save(car);
        GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);
        LineString lineString = gf.createLineString(new Coordinate[]{new Coordinate(-10, 50), new Coordinate(10, 50), new Coordinate(12, 62)});
        Ride ride = new Ride(cal.getTime(), 3);
        ride.setCar(car);
        ride.setUser(user);
        rideRepository.save(ride);
        Request request = new Request(RequestStatus.SENT);
        request.setRide(ride);
        request.setUser(user);
        requestRepository.save(request);
        UserAddRequestAnswerDtoRequest userAddRequestAnswerDtoRequest = new UserAddRequestAnswerDtoRequest(RequestStatus.ACCEPT);
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        String url = "http://localhost:" + port + "/api/requests/{id}";
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Access-Token", session.getToken());
        Map<String, Long> params = new HashMap<>();
        params.put("id", request.getId());
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
        HttpEntity<String> entity = new HttpEntity<String>(mapper.writeValueAsString(userAddRequestAnswerDtoRequest), headers);

        ResponseEntity<UserAddRequestAnswerDtoResponse> result = restTemplate.exchange(builder.buildAndExpand(params).toUri(),
                HttpMethod.POST, entity, UserAddRequestAnswerDtoResponse.class);
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertEquals(userAddRequestAnswerDtoRequest.getRequestStatus(), result.getBody().getRequest());
    }

    @Test
    public void testCancelRequest() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Session session = new Session("session");
        session.setUser(user);
        sessionRepository.save(session);
        Calendar cal = Calendar.getInstance();
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);
        carRepository.save(car);
        GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);
        LineString lineString = gf.createLineString(new Coordinate[]{new Coordinate(-10, 50), new Coordinate(10, 50), new Coordinate(12, 62)});
        Ride ride = new Ride(cal.getTime(), 3);
        ride.setCar(car);
        ride.setUser(user);
        rideRepository.save(ride);
        Request request = new Request(RequestStatus.SENT);
        request.setRide(ride);
        request.setUser(user);
        requestRepository.save(request);
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        String url = "http://localhost:" + port + "/api/requests/{id}";
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Access-Token", session.getToken());
        Map<String, Long> params = new HashMap<>();
        params.put("id", request.getId());
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<UserCancelRequestDtoResponse> result = restTemplate.exchange(builder.buildAndExpand(params).toUri(),
                HttpMethod.DELETE, entity, UserCancelRequestDtoResponse.class);
        Assert.assertEquals(200, result.getStatusCodeValue());
    }
*/
}

