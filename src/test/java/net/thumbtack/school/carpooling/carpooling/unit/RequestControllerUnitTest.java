package net.thumbtack.school.carpooling.carpooling.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.carpooling.controller.RequestController;
import net.thumbtack.school.carpooling.enums.RequestStatus;
import net.thumbtack.school.carpooling.model.Request;
import net.thumbtack.school.carpooling.request.UserAddRequestAnswerDtoRequest;
import net.thumbtack.school.carpooling.response.UserAddRequestAnswerDtoResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Class for unit testing RequestController endpoint
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = RequestController.class, secure = false)
public class RequestControllerUnitTest {
    @MockBean
    private RequestController requestController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;


    @Test
    public void testUserCreateRequest() throws Exception {
        MvcResult result = this.mvc.perform(
                post("/api/ride/{id}", 185)
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(null)))
                .andReturn();
        Assert.assertEquals(200, result.getResponse().getStatus());

    }

    @Test
    public void testAddRequestAnswer() throws Exception {
        UserAddRequestAnswerDtoRequest userAddRequestAnswerDtoRequest = new UserAddRequestAnswerDtoRequest(RequestStatus.ACCEPT);
        UserAddRequestAnswerDtoResponse userAddRequestAnswerDtoResponse = new UserAddRequestAnswerDtoResponse(1,RequestStatus.ACCEPT);
        Mockito.when(requestController.addRequestAnswer(any(HttpServletRequest.class), anyLong(), any(UserAddRequestAnswerDtoRequest.class))).thenReturn(userAddRequestAnswerDtoResponse);

        MvcResult result = this.mvc.perform(
                post("/api/requests/{id}", 100)
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(userAddRequestAnswerDtoRequest)))
                .andReturn();
        UserAddRequestAnswerDtoResponse userAddRequestAnswerDtoResponse2 = mapper.readValue(result.getResponse().getContentAsString(), UserAddRequestAnswerDtoResponse.class);
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertNotNull(userAddRequestAnswerDtoResponse2);
    }

}
