package net.thumbtack.school.carpooling.carpooling.database;

import net.thumbtack.school.carpooling.model.Car;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.CarRepository;
import net.thumbtack.school.carpooling.repository.SessionRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class for testing Car table in Database
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CarRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private CarRepository carRepository;



    @Test
    public void addCar() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);

        carRepository.save(car);
        Assert.assertNotNull(car.getId());
        Assert.assertEquals(car.getUser().getId(), user.getId());

    }
}
