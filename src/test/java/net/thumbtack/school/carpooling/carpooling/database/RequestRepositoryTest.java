package net.thumbtack.school.carpooling.carpooling.database;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.PrecisionModel;
import net.thumbtack.school.carpooling.enums.RequestStatus;
import net.thumbtack.school.carpooling.model.Car;
import net.thumbtack.school.carpooling.model.Request;
import net.thumbtack.school.carpooling.model.Ride;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;

/**
 * Class for testing Request table in Database
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RequestRepositoryTest {
/*
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private RequestRepository requestRepository;
    @Test
    public void userCreateRequestToRide() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Calendar cal = Calendar.getInstance();
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);
        carRepository.save(car);
        GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);
        LineString lineString = gf.createLineString(new Coordinate[]{new Coordinate(-10, 50), new Coordinate(10, 50), new Coordinate(12, 62)});
        Ride ride = new Ride(cal.getTime(), 3);
        ride.setCar(car);
        ride.setUser(user);
        rideRepository.save(ride);
        Request request = new Request(RequestStatus.SENT);
        request.setUser(user);
        request.setRide(ride);

        requestRepository.save(request);
        Assert.assertNotNull(request.getId());
    }
    */
}
