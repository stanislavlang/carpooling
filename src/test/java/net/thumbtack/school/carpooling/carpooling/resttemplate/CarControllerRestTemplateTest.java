package net.thumbtack.school.carpooling.carpooling.resttemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.SessionRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import net.thumbtack.school.carpooling.request.UserAddCarDtoRequest;
import net.thumbtack.school.carpooling.request.UserLoginDtoRequest;
import net.thumbtack.school.carpooling.response.UserAddCarDtoResponse;
import net.thumbtack.school.carpooling.response.UserLoginDtoResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for integration testing UserController endpoint
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CarControllerRestTemplateTest {

    @LocalServerPort
    private int port;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private ObjectMapper mapper;

    @Before
    public void init() {
        sessionRepository.deleteAll();
    }


    @Test
    public void testAddCar() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com","Петров Иван"));
        Session session = new Session("session");
        session.setUser(user);
        sessionRepository.save(session);
        UserAddCarDtoRequest userAddCarDtoRequest = new UserAddCarDtoRequest("Honda Accord", "red", "sedan", "Е666КХ");
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        String url = "http://localhost:" + port + "/api/car";
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Access-Token",session.getToken());
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
        HttpEntity<String> entity = new HttpEntity<String>(mapper.writeValueAsString(userAddCarDtoRequest), headers);
        ResponseEntity<UserAddCarDtoResponse> result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, entity, UserAddCarDtoResponse.class);
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertNotNull(result.getBody().getId());
        Assert.assertEquals(userAddCarDtoRequest.getColor(),result.getBody().getColor());
        Assert.assertEquals(userAddCarDtoRequest.getType(),result.getBody().getType());
        Assert.assertEquals(userAddCarDtoRequest.getLicensePlate(),result.getBody().getLicensePlate());
        Assert.assertEquals(userAddCarDtoRequest.getBrand(),result.getBody().getBrand());

    }

}

