package net.thumbtack.school.carpooling.carpooling.resttemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.carpooling.model.Car;
import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.*;
import net.thumbtack.school.carpooling.request.PointDto;
import net.thumbtack.school.carpooling.request.UserPostRideDtoRequest;
import net.thumbtack.school.carpooling.response.UserPostRideDtoResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Class for integration testing UserController endpoint
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RideControllerRestTemplateTest {

    @LocalServerPort
    private int port;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private StatisticsRepository statisticsRepository;
    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private PathRepository pathRepository;

    @Autowired
    private ObjectMapper mapper;

    @Before
    public void init() {
        sessionRepository.deleteAll();
        requestRepository.deleteAll();
        pathRepository.deleteAll();
        rideRepository.deleteAll();
        ratingRepository.deleteAll();
        statisticsRepository.deleteAll();
        carRepository.deleteAll();
        userRepository.deleteAll();

    }


    @Test
    public void testCreateRide() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Session session = new Session("session");
        session.setUser(user);
        sessionRepository.save(session);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        sdf.setTimeZone(TimeZone.getDefault());
        calendar.add(Calendar.DAY_OF_MONTH, +2);
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);
        carRepository.save(car);
        UserPostRideDtoRequest userPostRideDtoRequest = new UserPostRideDtoRequest(calendar.getTime(), 3, new PointDto(40.3434934, 44.5829085005027), new PointDto(40.5838854,
                44.3899401005149), car.getId());
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        String url = "http://localhost:" + port + "/api/ride";
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Access-Token", session.getToken());
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
        HttpEntity<String> entity = new HttpEntity<String>(mapper.writeValueAsString(userPostRideDtoRequest), headers);

        ResponseEntity<UserPostRideDtoResponse> result = restTemplate.exchange(builder.buildAndExpand().toUri(),
                HttpMethod.POST, entity, UserPostRideDtoResponse.class);
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertNotNull(result.getBody().getId());
    }

}

