package net.thumbtack.school.carpooling.carpooling.resttemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.carpooling.enums.RequestStatus;
import net.thumbtack.school.carpooling.enums.RideStatus;
import net.thumbtack.school.carpooling.model.*;
import net.thumbtack.school.carpooling.repository.*;
import net.thumbtack.school.carpooling.request.UserAddRateDtoRequest;
import net.thumbtack.school.carpooling.response.UserAddRateDtoResponse;
import net.thumbtack.school.carpooling.response.UserGetRatingDtoResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for integration testing UserController endpoint
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RatingControllerRestTemplateTest {

    @LocalServerPort
    private int port;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private StatisticsRepository statisticsRepository;
    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private PathRepository pathRepository;

    @Autowired
    private ObjectMapper mapper;

    @Before
    public void init() {
        sessionRepository.deleteAll();
        requestRepository.deleteAll();
        pathRepository.deleteAll();
        rideRepository.deleteAll();
        ratingRepository.deleteAll();
        statisticsRepository.deleteAll();
        carRepository.deleteAll();
        userRepository.deleteAll();

    }


    @Test
    public void testAddRating() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        User userForRate = userRepository.save(new User("petrov95@gmail.com", "Петров Иван"));
        Session session = new Session("session");
        session.setUser(user);
        sessionRepository.save(session);
        Car car = new Car("Honda Accord", "red", "sedan", "Е666КХ");
        car.setUser(user);
        carRepository.save(car);
        Calendar cal = Calendar.getInstance();
        Ride ride = new Ride(cal.getTime(), 3);
        ride.setCar(car);
        ride.setUser(user);
        ride.setRideStatus(RideStatus.FINISH);
        rideRepository.save(ride);
        Request request = new Request(RequestStatus.ACCEPT);
        request.setRide(ride);
        request.setUser(userForRate);
        requestRepository.save(request);
        UserAddRateDtoRequest userAddRateDtoRequest = new UserAddRateDtoRequest(5);
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        String url = "http://localhost:" + port + "/api/rides/{rideId}/users/{userId}/rate";
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Access-Token", session.getToken());
        Map<String, Long> params = new HashMap<>();
        params.put("userId", userForRate.getId());
        params.put("rideId", ride.getId());
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
        HttpEntity<String> entity = new HttpEntity<String>(mapper.writeValueAsString(userAddRateDtoRequest), headers);
        ResponseEntity<UserAddRateDtoResponse> result = restTemplate.exchange(builder.buildAndExpand(params).toUri(), HttpMethod.POST, entity, UserAddRateDtoResponse.class);
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertNotNull(result.getBody().getId());
        Assert.assertEquals(userAddRateDtoRequest.getRating(), result.getBody().getRating());
    }

    @Test
    public void testGetAvgRating() throws Exception {
        User user = userRepository.save(new User("petrov12@gmail.com", "Петров Иван"));
        User user2 = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Session session = new Session("session");
        session.setUser(user);
        sessionRepository.save(session);
        Rating rating = new Rating();
        rating.setEstimatedUser(user);
        rating.setEvaluatingUser(user2);
        rating.setRating(5);
        ratingRepository.save(rating);
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        String url = "http://localhost:" + port + "/api/user/rate";
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Access-Token", session.getToken());
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<UserGetRatingDtoResponse> result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.GET, entity, UserGetRatingDtoResponse.class);
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertNotNull(result.getBody().getId());
        Assert.assertNotNull(result.getBody().getRating());
    }

}

