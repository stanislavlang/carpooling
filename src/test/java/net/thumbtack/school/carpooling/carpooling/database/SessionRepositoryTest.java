package net.thumbtack.school.carpooling.carpooling.database;

import net.thumbtack.school.carpooling.model.Session;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.repository.SessionRepository;
import net.thumbtack.school.carpooling.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class for testing Session table in Database
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SessionRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Before
    public void init() {
        sessionRepository.deleteAll();
    }


    @Test
    public void addSession() throws Exception {
        User user = userRepository.save(new User("petrov@gmail.com", "Петров Иван"));
        Session session = new Session("session");
        session.setUser(user);
        User user2 = userRepository.save(new User("petrov123@gmail.com", "Петров Иван"));
        Session session2 = new Session("session2");
        session2.setUser(user2);

        sessionRepository.save(session);
        sessionRepository.save(session2);
        Assert.assertNotNull(session.getId());
    }


}
