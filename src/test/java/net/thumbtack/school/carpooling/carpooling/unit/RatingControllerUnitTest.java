package net.thumbtack.school.carpooling.carpooling.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.carpooling.controller.RatingController;
import net.thumbtack.school.carpooling.controller.RideController;
import net.thumbtack.school.carpooling.request.UserAddRateDtoRequest;
import net.thumbtack.school.carpooling.response.UserAddRateDtoResponse;
import net.thumbtack.school.carpooling.response.UserGetRatingDtoResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Class for unit testing RatingController endpoint
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = RatingController.class, secure = false)
public class RatingControllerUnitTest {
    @MockBean
    private RatingController ratingController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;


    @Test
    public void testUserAddRate() throws Exception {
        UserAddRateDtoRequest userAddRateDtoRequest = new UserAddRateDtoRequest(5);
        UserAddRateDtoResponse userAddRateDtoResponse = new UserAddRateDtoResponse((long) 1, 5);
        Mockito.when(ratingController.addRate(any(UserAddRateDtoRequest.class), any(HttpServletRequest.class), anyLong(),anyLong())).thenReturn(userAddRateDtoResponse);

        MvcResult result = this.mvc.perform(
                post("/api/rides/{rideId}/users/{userId}/rate", 100, 185)
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(userAddRateDtoRequest)))
                .andReturn();
        UserAddRateDtoResponse userAddRateDtoResponse2 = mapper.readValue(result.getResponse().getContentAsString(), UserAddRateDtoResponse.class);
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertEquals(userAddRateDtoResponse2.getId(), userAddRateDtoResponse.getId());
        Assert.assertEquals(userAddRateDtoResponse2.getRating(), userAddRateDtoResponse.getRating());
    }

    @Test
    public void testUserGetRate() throws Exception {
        UserGetRatingDtoResponse userGetRatingDtoResponse = new UserGetRatingDtoResponse((long) 1, "Petrov Dmitry", "petrov@mail.ru", 5);
        Mockito.when(ratingController.getRate(any(HttpServletRequest.class))).thenReturn(userGetRatingDtoResponse);

        MvcResult result = this.mvc.perform(
                get("/api/user/rate")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(null)))
                .andReturn();
        UserGetRatingDtoResponse userGetRatingDtoResponse2 = mapper.readValue(result.getResponse().getContentAsString(), UserGetRatingDtoResponse.class);
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertEquals(userGetRatingDtoResponse.getId(), userGetRatingDtoResponse2.getId());
        Assert.assertEquals(userGetRatingDtoResponse.getName(), userGetRatingDtoResponse2.getName());
        Assert.assertEquals(userGetRatingDtoResponse.getEmail(), userGetRatingDtoResponse2.getEmail());
        Assert.assertEquals(userGetRatingDtoResponse.getRating(), userGetRatingDtoResponse2.getRating());
    }
}
