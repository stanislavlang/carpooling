package net.thumbtack.school.carpooling.carpooling.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.carpooling.controller.RequestController;
import net.thumbtack.school.carpooling.controller.StatisticsController;
import net.thumbtack.school.carpooling.enums.RequestStatus;
import net.thumbtack.school.carpooling.model.Request;
import net.thumbtack.school.carpooling.model.User;
import net.thumbtack.school.carpooling.request.UserAddRateDtoRequest;
import net.thumbtack.school.carpooling.request.UserAddRequestAnswerDtoRequest;
import net.thumbtack.school.carpooling.response.UserAddRateDtoResponse;
import net.thumbtack.school.carpooling.response.UserAddRequestAnswerDtoResponse;
import net.thumbtack.school.carpooling.response.UserGetRatingDtoResponse;
import net.thumbtack.school.carpooling.response.UserGetStatisticsDtoResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * Class for unit testing StatisticsController endpoint
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = StatisticsController.class, secure = false)
public class StatisticsControllerUnitTest {
    @MockBean
    private StatisticsController statisticsController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;


    @Test
    public void testUserGetStatistics() throws Exception {
        UserGetStatisticsDtoResponse userGetStatisticsDtoResponse = new UserGetStatisticsDtoResponse((long) 5, new User(), 5);
        Mockito.when(statisticsController.getStatistics(any(HttpServletRequest.class))).thenReturn(userGetStatisticsDtoResponse);

        MvcResult result = this.mvc.perform(
                get("/api/statistics")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(null)))
                .andReturn();
        UserGetStatisticsDtoResponse userGetStatisticsDtoResponse2 = mapper.readValue(result.getResponse().getContentAsString(), UserGetStatisticsDtoResponse.class);
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertEquals(userGetStatisticsDtoResponse.getId(), userGetStatisticsDtoResponse2.getId());
        Assert.assertEquals(userGetStatisticsDtoResponse.getRidesCount(), userGetStatisticsDtoResponse2.getRidesCount());
    }

}
